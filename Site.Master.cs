﻿using sklepmaster.baza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonZaloguj_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/Logowanie.aspx");
            //sklepmaster.baza.DaneZalogowanejOsoby osoba = new DaneZalogowanejOsoby();
            //labelzalogowany.Text = "  spadaj " + osoba.Imie + "  " ;
        }
        protected void StronaGluowna_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/Starowa.aspx");
            
        }

        

        protected void buttonWyloguj_Click(object sender, EventArgs e)
        {
            if (Session["klient"] == null) return;
            Session.Remove("klient");
            Session.Abandon();
            //labelWiadomosc.Text = "Wylogowano";
            Page.Response.Redirect("/Account/Starowa.aspx");
            
        }

        protected void buttonZarządzanieKontem_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/EdycjaDanych.aspx");
        }

        protected void buttonDodajPracownika_Click(object sender, EventArgs e)
        {

        }

		protected void buttonDodajKategorie_Click(object sender, EventArgs e)
		{
			Page.Response.BufferOutput = true;
			Page.Response.Redirect("/Account/DodawanieKategorii.aspx");
		}

        protected void buttonDodajProdukt_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/DodawanieProduktu.aspx");
        }

        protected void buttonEdytujProdukt_Click(object sender, EventArgs e)
        {
            
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/EdycjaProduktu1.aspx");
        }
    }
}