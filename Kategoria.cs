﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace sklepmaster
{
	[DebuggerStepThrough()]
	public class Kategoria
	{
		public string Nazwa { get; set; }
		public int Id { get; set; }

		[DebuggerStepThrough()]
		public Kategoria()
		{

		}

		[DebuggerStepThrough()]
		public Kategoria(string nazwa, int id)
		{
			this.Nazwa = nazwa;
			this.Id = id;
		}
	}
}