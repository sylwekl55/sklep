﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class EdycjaProduktu1Szczegoly : System.Web.UI.Page
    {
        int IdProduktu;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Label1.Text = this.ClientQueryString;

            

            if (!Page.IsPostBack)
            {
                this.BindListView();
                this.BindListView1();
                //this.BindListView2();
                
            }
        }
        private void BindListView()
        {
            DataTable dataTable = new DataTable();

            sklepmaster.baza.DaneProduktu dane = new sklepmaster.baza.DaneProduktu();

            dane.IdProduktu = Convert.ToInt32(this.ClientQueryString);

            string sqlCommand = "SELECT id_fotografii, zdjecie FROM fotografia WHERE produkt=" + Convert.ToInt32(this.ClientQueryString) + " AND zdjecie is not null";

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }
            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }



            lvCustomers.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            lvCustomers.DataBind();
        }


        private void BindListView1()
        {
            DataTable dataTable = new DataTable();
            DataTable dataTablePromocja = new DataTable();
            DataTable dataTableKategoria = new DataTable();

            string sqlCommand = "SELECT nazwa_produktu, ilosc_w_magazynie, wersja, opis, cena_brutto, producent, vat, promocje, nazwa_firmy, nip, regon, kraj, kod_pocztowy, wojewodztwo, nr_mieszkania, email, miejscowosc, numer, kategoria FROM sklep.produkt p, producent pp, adres a WHERE p.producent=pp.id_producent AND a.id_adresu=pp.adres AND id_produktu=" + Convert.ToInt32(this.ClientQueryString) + ";";
            string sqlCommandPromocja = "SELECT id_promocji, nazwa_promocji, data_zaczecia_promocji, data_zakonczenia_promocji from promocje";
            string sqlCommandKategoria = "SELECT id_kategoria, nazwa from kategoria";

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }

                        using (var commanda = new MySqlCommand(sqlCommandPromocja, polaczenie))
                        {
                            using (var adapter = new MySqlDataAdapter(commanda))
                            {
                                adapter.Fill(dataTablePromocja);
                            }
                        }

                        using (var commandk = new MySqlCommand(sqlCommandKategoria, polaczenie))
                        {
                            using (var adapter = new MySqlDataAdapter(commandk))
                            {
                                adapter.Fill(dataTableKategoria);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }

            // pole wyświetlane kategorii
            DropDownListKategoria.DataTextField = "nazwa";
            // pole ukryte
            DropDownListKategoria.DataValueField = "id_kategoria";
            // źródło danych 
            DropDownListKategoria.DataSource = dataTableKategoria;
            DropDownListKategoria.DataBind();


            // pole wyświetlane promocjii
            dataTablePromocja.Columns.Add(new DataColumn("Promocjee", System.Type.GetType("System.String"), "nazwa_promocji + ' rozpoczęcie: ' + data_zaczecia_promocji + ' zakończenie: ' + data_zakonczenia_promocji"));

            DropDownListPromocja.DataTextField = "Promocjee";
            // pole ukryte
            DropDownListPromocja.DataValueField = "id_promocji";
            // źródło danych 
            DropDownListPromocja.DataSource = dataTablePromocja;
            DropDownListPromocja.DataBind();


            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }
            DataRow rekord = dataTable.Rows[0];

            sklepmaster.baza.DaneProduktu dane = new sklepmaster.baza.DaneProduktu();

            IdProduktu = Convert.ToInt32(this.ClientQueryString);
            dane.NazwaFirmy =(string)rekord[sklepmaster.baza.pola.Producent.NazwaFirmy];
            dane.NazwaProduktu = (string)rekord[sklepmaster.baza.pola.Produkt.NazwaProduktu];
            dane.IloscWMagazynie = (int)rekord[sklepmaster.baza.pola.Produkt.IloscWMagazynie];
            dane.Wersja = (string)rekord[sklepmaster.baza.pola.Produkt.Wersja];
            dane.Opis = (string)rekord[sklepmaster.baza.pola.Produkt.Opis];
            dane.CenaBrutto = (float)rekord[sklepmaster.baza.pola.Produkt.CenaBrutto];
           // dane.Ocena = (int)rekord[sklepmaster.baza.pola.Produkt.Ocena];
            dane.Producent = (int)rekord[sklepmaster.baza.pola.Produkt.Producent];
            dane.Vat = (int)rekord[sklepmaster.baza.pola.Produkt.Vat];
            dane.Promocje = (int)rekord[sklepmaster.baza.pola.Produkt.Promocje];
            dane.Kategoria = (int)rekord[sklepmaster.baza.pola.Produkt.Kategoria];


            Session["IdProduktu"] = Convert.ToInt32(this.ClientQueryString);
            Session["NazwaFirmy"] = (string)rekord[sklepmaster.baza.pola.Producent.NazwaFirmy];
            Session["Opis"] = (string)rekord[sklepmaster.baza.pola.Produkt.Opis];
            Session["CenaBrutto"] = (float)rekord[sklepmaster.baza.pola.Produkt.CenaBrutto];
            Session["IloscWMagazynie"] = (int)rekord[sklepmaster.baza.pola.Produkt.IloscWMagazynie];
            Session["Kategoria"] = (int)rekord[sklepmaster.baza.pola.Produkt.Kategoria];
            Session["Promocje"] = (int)rekord[sklepmaster.baza.pola.Produkt.Promocje];
        
            ListView1.DataSource = dataTable;
            ListView1.DataBind();

        }

        protected void ButtonEdytuj_Click(object sender, EventArgs e)
        {
            float cena_brutto;
            int ilosc_w_magazynie, promocje, kategoria;

            sklepmaster.baza.DaneProduktu dane = new sklepmaster.baza.DaneProduktu();


            if (!(Int32.TryParse(TextBoxIloscWMagazynie.Text, out ilosc_w_magazynie))) //konwertuje na int z stringa 
            {
                ilosc_w_magazynie = 0;
            }

            if (!(float.TryParse(TextBoxCena.Text, out cena_brutto))) //konwertuje na int z stringa 
            {
                cena_brutto = 0;
            }

            if (!(Int32.TryParse(DropDownListPromocja.SelectedValue, out promocje)))
            {
                promocje = 0;
            }
            

            if (!(Int32.TryParse(DropDownListKategoria.SelectedValue, out kategoria)))
            {
                kategoria = 0;
            }

            //-------------------------------------------------
            dane.IdProduktu = (int)Session["IdProduktu"];
            
            if (TextBoxNazwaProduktu.Text.Length != 0)
            {
                dane.NazwaFirmy = TextBoxNazwaProduktu.Text;
            }
            else { dane.NazwaFirmy = (string)Session["NazwaFirmy"];  }

            if (TextBoxOpis.Text.Length != 0)
            {
                dane.Opis = TextBoxOpis.Text;
            }
            else { dane.Opis = (string)Session["Opis"]; }

            if (cena_brutto != 0)
            {
                dane.CenaBrutto = cena_brutto;
            }
            else { dane.CenaBrutto = (float)Session["CenaBrutto"]; }

            if (ilosc_w_magazynie != 0)
            {
                dane.IloscWMagazynie = ilosc_w_magazynie;
            }
            else { dane.IloscWMagazynie = (int)Session["IloscWMagazynie"]; }

            if (kategoria != 1)
            {
                dane.Kategoria = kategoria;
            }
            else { dane.Kategoria = (int)Session["Kategoria"]; }

            if (promocje != 1)
            {
                dane.Promocje = promocje;
            }
            else { dane.Promocje = (int)Session["Promocje"]; }
            //------------------------------------------------------------------------------------------------------

            try
            {
                if (!dane.Edytuj())
                {
                    labelBlad.Text = "Błąd dodawania klienta: " + dane.PowodNieZarejestrowania;
                    return;
                }

                labelBlad.Text = "Zmieniono Dane Osobowe ";
            }
            catch (Exception ex)
            {
                labelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
                return;
            }
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/EdycjaProduktu1.aspx");
        }
    }

}