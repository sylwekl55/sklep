﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EdycjaDanych.aspx.cs" Inherits="sklepmaster.Account.EdycjaDanych" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 317px;
        }
        .auto-style10 {
			width: 491px;
		}
		.auto-style11 {
			width: 490px;
		}
        .tabele {
            border-radius: 16px !important;
            border="0" ;
            border-spacing: 0mm 2mm;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>&nbsp;</h2>
   
    <h4>Zarządzanie kontem</h4>
    <hr />
	
		<table class=" tabele auto-style1">
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelHaslo">Hasło</span>
				
						<asp:TextBox ID="TextBoxHaslo" TextMode="Password" runat="server" class="form-control" placeholder="Hasło" aria-describedby="labelHaslo" style="width: 250px;"></asp:TextBox>
					</div>
				</td>
				<td>
                    
				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<asp:CompareValidator class="label label-danger" ID="CompareValidator1" runat="server" ControlToCompare="TextBoxHaslo" ControlToValidate="TextBoxPowtorzHaslo" ErrorMessage="hasła nie są jednakowe"></asp:CompareValidator>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelPowtorzHaslo">Powtórz Hasło</span>
						<asp:TextBox ID="TextBoxPowtorzHaslo"  runat="server" class="form-control" placeholder="Powtórz Hasło" aria-describedby="labelHaslo" style="width: 250px;" TextMode="Password"></asp:TextBox>
					</div>
				</td>
				<td>

				</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelNazwaFirmy">Nazwa firmy</span>
						<asp:TextBox ID="TextBoxNazwaFirmy"  runat="server" class="form-control" placeholder="Nazwa firmy" aria-describedby="labelNazwaFirmy" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td>
                    <asp:Label ID="LabelNazwaFirmy1" runat="server" Text=""></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelRegon">Regon</span>
						<asp:TextBox ID="TextBoxRegon"  runat="server" class="form-control" placeholder="Regon" aria-describedby="labelRegon" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td>
                    <asp:Label ID="labelRegon1" runat="server" Text=""></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelNIP">NIP</span>
						<asp:TextBox ID="TextBoxNIP"  runat="server" class="form-control" placeholder="NIP" aria-describedby="labelNIP" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td>
                    <asp:Label ID="labelNIP1" runat="server" Text=""></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		
	<table class=" tabele auto-style1" border="0">
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelUlica">Ulica</span>
					<asp:TextBox ID="TextBoxUlica" runat="server" class="form-control" placeholder="Ulica" aria-describedby="labelUlica" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelUlica1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>

			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrLokalu">Nr.Lokalu</span>
					<asp:TextBox ID="TextBoxNrLokalu" runat="server" class="form-control" placeholder="Nr.Lokalu" aria-describedby="labelNrLokalu" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelNrLokalu1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrMieszkania">Nr.Mieszkania</span>
					<asp:TextBox ID="TextBoxNrMieszkania" runat="server" class="form-control" placeholder="Nr.Mieszkania" aria-describedby="labelNrMieszkania" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelNrMieszkania1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKodPocztowy">Kod Pocztowy</span>
					<asp:TextBox ID="TextBoxKodPocztowy" runat="server" class="form-control" placeholder="Kod Pocztowy" aria-describedby="labelKodPocztowy" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelKodPocztowy1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelMiejscowosc">Miejscowość</span>
					<asp:TextBox ID="TextBoxMiejscowosc" runat="server" class="form-control" placeholder="Miejscowość" aria-describedby="labelMiejscowosc" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                 <asp:Label ID="labelMiejscowosc1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelPowiat">Powiat</span>
					<asp:TextBox ID="TextBoxPowiat" runat="server" class="form-control" placeholder="Powiat" aria-describedby="labelPowiat" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelPowiat1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelWojewudztwo">Województwo</span>
					<asp:TextBox ID="TextBoxWojewudztwo" runat="server" class="form-control" placeholder="Województwo" aria-describedby="labelWojewudztwo" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelWojewudztwo1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKraj">Kraj</span>
					<asp:TextBox ID="TextBoxKraj" runat="server" class="form-control" placeholder="Kraj" aria-describedby="labelKraj" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelKraj1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrTelefonu">Nr.Telefonu</span>
					<asp:TextBox ID="TextBoxNrTelefonu" runat="server" class="form-control" placeholder="Nr.Telefonu" aria-describedby="labelNrTelefonu" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:Label ID="labelNrTelefonu1" runat="server" Text=""></asp:Label>
			</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">
                <p>
                <asp:Button class="btn btn-default btn-primary" ID="ButtonZmien" runat="server" Text="Zmień" OnClick="ButtonZmien_Click" />
                </p>

			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
    <p>
        <asp:Label ID="labelBlad" runat="server" BorderColor="White" ForeColor="White"></asp:Label>
    </p>
    
    

</asp:Content>
