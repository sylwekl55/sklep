﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using System.Data.SqlClient;

namespace sklepmaster.Account
{
    public partial class StartowaWyszukaj : System.Web.UI.Page
    {
        public int nazwa { set; get; }

        protected void Button_Nazwa_Click(object sender, EventArgs e)
        {
            nazwa = 1;
            Response.Cookies["nazwa"].Value = Convert.ToString(nazwa);
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }
        protected void Button_Cena_Rosnaco_Click(object sender, EventArgs e)
        {
            nazwa = 2;
            Response.Cookies["nazwa"].Value = Convert.ToString(nazwa);
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }
        protected void Button_Cena_Malejanco_Click(object sender, EventArgs e)
        {
            nazwa = 3;
            Response.Cookies["nazwa"].Value = Convert.ToString(nazwa);
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }
        protected void Button_Ilosc_Click(object sender, EventArgs e)
        {
            nazwa = 4;
            Response.Cookies["nazwa"].Value = Convert.ToString(nazwa);
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Labelszukaj.Text = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            nazwa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["nazwa"].Value));
            

            
            switch(nazwa)
            {
                case 1:
                    {
                        
                            _WczytajKategorie();
                            this.BindListViewNazwa();
                       
                        break;
                    }
                case 2:
                    {
                        
                            _WczytajKategorie();
                            this.BindListViewCenaRosnaco();
                        
                        break;
                    }
                case 3:
                    {
                        
                            _WczytajKategorie();
                            this.BindListViewCenaMalejaco();
                       
                        break;
                    }
                case 4:
                    {

                        _WczytajKategorie();
                        this.BindListViewIloscDostepna();

                        break;
                    }
                default:
                    {
                        
                         _WczytajKategorie();
                        this.BindListView();
                        
                        break;
                    }
            }

            
            
        }


        private void BindListViewIloscDostepna()
        {
            DataTable dataTable = new DataTable();

            // szukajz kategorii
            int szukajkategoriel = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value));
            string szukajkategorie, kategoriaoperator;
            szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
            if (szukajkategoriel <= 0)
            {
                kategoriaoperator = "";
                szukajkategorie = "is not null";
            }
            else
            {
                kategoriaoperator = "=";
                szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
                Convert.ToInt32(szukajkategorie);
            }


            DataSet dataSet1 = new DataSet();

            string szukaj = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            string zmiennaszukaj;
            //textbox szukaj 
            if (TextBoxSzukaj == null)
            {
                zmiennaszukaj = "%";
            }
            else
            {
                zmiennaszukaj = szukaj;
            }
            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii and nazwa_produktu like '%" + zmiennaszukaj + "%' and kategoria " + kategoriaoperator + " " + szukajkategorie + " union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null";
            
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
            // -----------sortowanie ------
            // ustaw źrudło danych BindingSource1.

            DataView view = new DataView(dataTable);
            view.Sort = "ilosc_w_magazynie DESC";

            lvCustomers.DataSource = view;
            lvCustomers.DataBind();
        }


        private void BindListViewCenaMalejaco()
        {
            DataTable dataTable = new DataTable();

            // szukajz kategorii
            int szukajkategoriel = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value));
            string szukajkategorie, kategoriaoperator;
            szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
            if (szukajkategoriel <= 0)
            {
                kategoriaoperator = "";
                szukajkategorie = "is not null";
            }
            else
            {
                kategoriaoperator = "=";
                szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
                Convert.ToInt32(szukajkategorie);
            }


            DataSet dataSet1 = new DataSet();

            string szukaj = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            string zmiennaszukaj;
            //textbox szukaj 
            if (TextBoxSzukaj == null)
            {
                zmiennaszukaj = "%";
            }
            else
            {
                zmiennaszukaj = szukaj;
            }
            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii and nazwa_produktu like '%" + zmiennaszukaj + "%' and kategoria " + kategoriaoperator + " " + szukajkategorie + " union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null";
            

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
            // -----------sortowanie ------
            // ustaw źrudło danych BindingSource1.

            DataView view = new DataView(dataTable);
            view.Sort = "cena_brutto DESC";

            lvCustomers.DataSource = view;
            lvCustomers.DataBind();
        }
        private void BindListViewCenaRosnaco()
        {
            DataTable dataTable = new DataTable();

            // szukajz kategorii
            int szukajkategoriel = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value));
            string szukajkategorie, kategoriaoperator;
            szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
            if (szukajkategoriel <= 0)
            {
                kategoriaoperator = "";
                szukajkategorie = "is not null";
            }
            else
            {
                kategoriaoperator = "=";
                szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
                Convert.ToInt32(szukajkategorie);
            }


            DataSet dataSet1 = new DataSet();

            string szukaj = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            string zmiennaszukaj;
            //textbox szukaj 
            if (TextBoxSzukaj == null)
            {
                zmiennaszukaj = "%";
            }
            else
            {
                zmiennaszukaj = szukaj;
            }
            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii and nazwa_produktu like '%" + zmiennaszukaj + "%' and kategoria " + kategoriaoperator + " " + szukajkategorie + " union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null";
            
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
            // -----------sortowanie ------
            // ustaw źrudło danych BindingSource1.

            DataView view = new DataView(dataTable);
            view.Sort = "cena_brutto asc";

            lvCustomers.DataSource = view;
            lvCustomers.DataBind();
        }

        private void BindListViewNazwa()
        {
            DataTable dataTable = new DataTable();

            // szukajz kategorii
            int szukajkategoriel = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value));
            string szukajkategorie, kategoriaoperator;
            szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
            if (szukajkategoriel <= 0)
            {
                kategoriaoperator = "";
                szukajkategorie = "is not null";
            }
            else
            {
                kategoriaoperator = "=";
                szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
                Convert.ToInt32(szukajkategorie);
            }


            DataSet dataSet1 = new DataSet();

            string szukaj = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            string zmiennaszukaj;
            //textbox szukaj 
            if (TextBoxSzukaj == null)
            {
                zmiennaszukaj = "%";
            }
            else
            {
                zmiennaszukaj = szukaj;
            }
            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii and nazwa_produktu like '%" + zmiennaszukaj + "%' and kategoria " + kategoriaoperator + " " + szukajkategorie + " union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null";
            
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
            // -----------sortowanie ------
            // ustaw źrudło danych BindingSource1.

            DataView view = new DataView(dataTable);
            view.Sort = "nazwa_produktu asc";

            lvCustomers.DataSource = view;
            lvCustomers.DataBind();
        }

        private void BindListViewSzukaj()
        {
            DataTable dataTable = new DataTable();
            DataSet dataSet1 = new DataSet();

            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null;";

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                            adapter.Fill(dataSet1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
            // -----------sortowanie ------
            // ustaw źrudło danych BindingSource1.

            DataView view = new DataView(dataTable);
            view.Sort = "nazwa_produktu asc";
            
            //DataRow[] foundRows;



            //foundRows = dataSet1.Tables["produkt"].Select("nazwa_produktu Like 'spadaj'");

           

            // ------------------przepisanie z dataset do datatable
            while (dataSet1.Tables.Count > 0)
            {
                DataTable table = dataSet1.Tables[0];
                if (dataSet1.Tables.CanRemove(table))
                {
                    dataSet1.Tables.Remove(table);
                }
            }
            // koniec przepisania

            lvCustomers.DataSource = view;
            lvCustomers.DataBind();
        }





        //----------------------------------------------------------------------------wczytaj
        private void BindListView()
        {

            DataTable dataTable = new DataTable();

            // szukajz kategorii
            int szukajkategoriel = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value));
            string szukajkategorie, kategoriaoperator;
            szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
            if(szukajkategoriel<=0)
            {
                kategoriaoperator = "";
                szukajkategorie = "is not null";
            }
            else
            {
                kategoriaoperator = "=";
                szukajkategorie = Server.HtmlEncode(Request.Cookies["szukajkategoria"].Value);
                Convert.ToInt32(szukajkategorie);
            }



            string szukaj = Server.HtmlEncode(Request.Cookies["szukaj"].Value);
            string zmiennaszukaj;
            //textbox szukaj 
            if (TextBoxSzukaj == null)
            {
                zmiennaszukaj = "%";
            }
            else
            {
                zmiennaszukaj = szukaj;
            }
            string sqlCommand = "SELECT id_produktu, nazwa_produktu, zdjecie, opis, ilosc_w_magazynie, cena_brutto FROM sklep.produkt, sklep.fotografia where sklep.produkt.id_produktu = sklep.fotografia.id_fotografii and nazwa_produktu like '%" + zmiennaszukaj + "%' and kategoria " + kategoriaoperator + " " + szukajkategorie + " union SELECT  id_produktu, nazwa_produktu, nazwa_produktu brakzdjecia, opis, ilosc_w_magazynie, cena_brutto from sklep.produkt  where sklep.produkt.zdjecie_start is null";
            
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }



            //string mojePolaczenie =
            //"SERVER=localhost;" +
            //"PORT=3306;" +
            //"DATABASE=sklep;" +
            //"UID=sylwek;" +
            //"PASSWORD=sylwekl5";   

            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }



            lvCustomers.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            lvCustomers.DataBind();

            //DropDownList1.DataTextField = "vat";

            // pole ukryte
            //DropDownList1.DataValueField = "id_vat";

            // źródło danych 
            //DropDownList1.DataSource = dataTable;

            //DropDownList1.DataBind();
        }


        public System.Drawing.Image byteArrayToImage(System.Byte[] ByteInArray)
        {
            MemoryStream ms = new MemoryStream(ByteInArray);
            System.Drawing.Image returnimage = System.Drawing.Image.FromStream(ms);
            return returnimage;

        }

        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (lvCustomers.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.BindListView();
        }




        protected void listaKategorii_SelectedNodeChanged(object sender, EventArgs e)
        {

        }
        private List<WezelKategorii> _PobierzKategorie()
        {
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();

                    return WezelKategorii.PobierzWszystkie(polaczenie);
                }
            }
            catch (Exception ex)
            {
                labelError.Text = "Błąd pobierania kategorii [" + ex.Message + "]";
                return null;
            }
        }


        private void _WczytajKategorie()
        {
            List<WezelKategorii> kategorie = _PobierzKategorie();

            if (kategorie == null)
            {
                return;
            }

            //listaKategorii.DataSource = kategorie;
            foreach (WezelKategorii wezelBazowy in kategorie)
            {
                listaKategorii.Nodes.Add(wezelBazowy.TreeWezel);
            }

            listaKategorii.Attributes.Add("onclick", "client_OnTreeNodeChecked(event)");
            listaKategorii.ExpandAll();
        }

        protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Logowanie.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/Logowanie.aspx");
        }

        protected void ButtonSzukaj_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }

        public string Obraz(object img)
        {
            if (img is Nullable)
            {
                return "<img src=\"~images/brak_foty.jpg\" />";
            }
            return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            string szukaj = TextBoxszukajj.Text;
            Response.Cookies["szukaj"].Value = szukaj;
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }

        protected void ButtonSzukajKategorie_Click(object sender, EventArgs e)
        {
            TreeNode zaznaczonaKategoria = null;
            int zaznaczonakategoria;

            if (listaKategorii.CheckedNodes.Count > 0)
            {
                if (listaKategorii.CheckedNodes.Count > 1)
                {
                    Labelbladszukajkategorie.Text = "proszę zaznaczyń tylko jedną kategorie";
                    return;
                }
                zaznaczonaKategoria = listaKategorii.CheckedNodes[0];
            }
            else
            {
                Labelbladszukajkategorie.Text = "zaznacz jedną kategorie której wyszukujesz a następnie kliknij szukaj";
                return;
            }
            Response.Cookies["szukajkategoria"].Value = zaznaczonaKategoria.Value;
            Page.Response.Redirect("/Account/StartowaWyszukaj.aspx");
        }

        

        

        

    }
}