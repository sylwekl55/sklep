﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
	public partial class DodajProducenta : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void ButtonDodaj_Click(object sender, EventArgs e)
		{
			sklepmaster.baza.DodawanieProducentow r = new sklepmaster.baza.DodawanieProducentow();

			int nip, regon, nr_lokalu, nr_mieszkania, numer;

			if (!(Int32.TryParse(TextBoxNIP.Text, out nip))) //konwertuje na int z stringa 
			{
				labelBlad.Text = "Nieprawidłowy numer nip";
				return;
			}

			if (!(Int32.TryParse(TextBoxREGON.Text, out regon))) //konwertuje na int z stringa 
			{
				labelBlad.Text = "Nieprawidłowy numer regon";
				return;
			}
			if (!(Int32.TryParse(TextBoxNrLokalu.Text, out nr_lokalu))) //konwertuje na int z stringa 
			{
				labelBlad.Text = "Nieprawidłowy numer lokalu";
				return;
			}

			if (!(Int32.TryParse(TextBoxNrMieszkania.Text, out nr_mieszkania)))
			{
				labelBlad.Text = "Nieprawidłowy numer mieszkania";
				return;
			}

			if (!(Int32.TryParse(TextBoxNrTelefonu.Text, out numer)))
			{
				labelBlad.Text = "Nieprawidłowy numer telefonu";
				return;
			}

			r.NazwaFirmy = TextBoxNazwaFirmy.Text;
			r.Nip = nip;
			r.Regon = regon;
			r.Email = TextBoxEmail.Text;
			r.KodPocztowy = TextBoxKodPocztowy.Text;
			r.Kraj = TextBoxKraj.Text;
			r.NazwaFirmy = TextBoxNazwaFirmy.Text;
			r.NrLokalu = nr_lokalu;
			r.NrMieszkania = nr_mieszkania;
			r.Numer = numer;
			r.Powiat = TextBoxPowiat.Text;
			r.Ulica = TextBoxUlica.Text;
			r.Wojewodztwo = TextBoxWojewudztwo.Text;
			r.Miejscowosc = TextBoxMiejscowosc.Text;

			try
			{
				if (!r.RejestrujProducenta())
				{
					labelBlad.Text = "Błąd dodawania Producenta pp: " + r.PowodNieZarejestrowania;
					return;
				}

				labelBlad.Text = "Zarejestrowano ";
			}
			catch (Exception ex)
			{
				labelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
				return;
			}
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/DodawanieProduktu.aspx");

		}
	}
}