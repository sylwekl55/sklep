﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DodajPracownika.aspx.cs" Inherits="sklepmaster.Account.DodajPracownika1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            width: 317px;
            height: 26px;
        }
        .auto-style4 {
            height: 26px;
        }
        .auto-style2 {
            width: 317px;
        }
        .auto-style5 {
            width: 317px;
            height: 25px;
        }
        .auto-style6 {
            height: 25px;
        }
        .auto-style8 {
            width: 317px;
            height: 24px;
        }
        .auto-style9 {
            height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><%: Title %></h2>
<h4>Dodaj Pracownika</h4>
<table class="auto-style1" >
	<tr>
		<td class="auto-style3">Imię</td>
		<td class="auto-style4">
			<asp:TextBox ID="TextBoxImie" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">Nazwisko<br />
			Email</td>
		<td>
			<asp:TextBox ID="TextBoxNazwisko" runat="server"></asp:TextBox>
			<br />
			<asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">Login</td>
		<td>
			<asp:TextBox ID="TextBoxLogin" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">Hasło</td>
		<td>
			<asp:TextBox ID="TextBoxHaslo" runat="server" TextMode="Password"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">
			<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxHaslo" ControlToValidate="TextBoxPowtorzHaslo" ErrorMessage="hasła nie są jednakowe"></asp:CompareValidator>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Powtórz Hasło</td>
		<td>
			<asp:TextBox ID="TextBoxPowtorzHaslo" runat="server" TextMode="Password"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Nazwa firmy</td>
		<td>
			<asp:TextBox ID="TextBoxNazwaFirmy" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style5">REGON</td>
		<td class="auto-style6">
			<asp:TextBox ID="TextBoxRegon" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style3">NIP</td>
		<td class="auto-style4">
			<asp:TextBox ID="TextBoxNIP" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<table class="auto-style1" border="1" dir="ltr">
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>
			&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Ulica</td>
		<td>
			<asp:TextBox ID="TextBoxUlica" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Nr.lokalu</td>
		<td>
			<asp:TextBox ID="TextBoxNrLokalu" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style8">Nr.Mieszkania</td>
		<td class="auto-style9">
			<asp:TextBox ID="TextBoxNrMieszkania" runat="server"></asp:TextBox>
		</td>
		<td class="auto-style9">&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style8">Kod pocztowy</td>
		<td class="auto-style9">
			<asp:TextBox ID="TextBoxKodPocztowy" runat="server"></asp:TextBox>
		</td>
		<td class="auto-style9"></td>
	</tr>
	<tr>
		<td class="auto-style2">Miejscowość</td>
		<td>
			<asp:TextBox ID="TextBoxMiejscowosc" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Powiat</td>
		<td>
			<asp:TextBox ID="TextBoxPowiat" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Województwo</td>
		<td>
			<asp:TextBox ID="TextBoxWojewudztwo" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Kraj</td>
		<td>
			<asp:TextBox ID="TextBoxKraj" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">Nr.telefonu</td>
		<td>
			<asp:TextBox ID="TextBoxNrTelefonu" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td class="auto-style2">Nr. służbowy</td>
		<td>
			<asp:TextBox ID="TextBoxNrSluzbowy" runat="server"></asp:TextBox>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style2">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<p>
	&nbsp;</p>
	<p>
	<asp:Label ID="labelBlad" runat="server" Text=""></asp:Label>
</p>
<p>
	<asp:Button ID="ButtonZarejestruj" runat="server" Text="Zarejestruj" OnClick="ButtonZarejestruj_Click" />
</p>
</asp:Content>
