﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EdycjaProduktu1.aspx.cs" Inherits="sklepmaster.Account.EdycjaProduktu1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style3 {
            width: 159px;
        }
        .tabele {
            border-radius: 16px !important;
            border="0" ;
            border-spacing: 0mm 5mm;
        }
        .table-produkty{
            border-radius: 16px;
            background-color: #caf5ff;
        }
        .table-produkty td{
            vertical-align: middle !important;/*important aby inne style nie nadpisywały*/
        }
        .table-produkty th{
            padding: 0px !important;
            background-color: #8adff2;
        }
        .table-produkty input{
            width: 100%;
            height: 84px;
            border:none;
            background-color: #8adff2;
        }
    </style>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="  auto-style1" >
        <tr>
            <td class="auto-style3" style="vertical-align:text-top;">
                <div class="input-group">
				</div>
                <div>
                </div>

                <div>
                </div>
                <asp:TreeView ID="listaKategorii" runat="server" OnSelectedNodeChanged="listaKategorii_SelectedNodeChanged" ShowCheckBoxes="All" Width="241px" ImageSet="Arrows" ShowLines="True">
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                    <ParentNodeStyle Font-Bold="False" />
                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
                </asp:TreeView>
                </td>
            <td>
                <br />

                <asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1"
                    ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging">
                    <LayoutTemplate>
                        <table class="tabele" border="0" style="width: 100%; height: inherit;">
                            <tr>
                                <th></th>
                                <th>
                                    <asp:Button ID="Button_Nazwa" runat="server"  Text="Nazwa" OnClick="Button_Nazwa_Click" />
                                </th>
                                <th><asp:Button style="width: 90px;" ID="Button1_ilosc_dostempna" runat="server"  Text="Ilość dostępna" />
                                </th>
                                <th>
                                    <asp:Button style="width: 52px;" ID="Button_cena" runat="server"  Text="Cena ↑" OnClick="Button_Cena_Rosnaco_Click" />
                                    <asp:Button style="width: 52px;" ID="Button_cena_malejanco" runat="server"  Text="Cena ↓" OnClick="Button_Cena_Malejanco_Click" />
                                </th>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                            <tr>
                                <td colspan="3">
                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
                                        <Fields>
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                ShowNextPageButton="false" />
                                            <asp:NumericPagerField ButtonType="Link" />
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <tr>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                        </tr>
                    </GroupTemplate>
                    <ItemTemplate>
                        <td>
                            <asp:ImageButton Height="113px" Width="150px" runat="server" PostBackUrl='<%# String.Format("EdycjaProduktu1Szczegoly.aspx?{0}", Eval("id_produktu")) %>' ImageUrl='<%# Obraz(Eval("zdjecie"))  %> ' />
                            <%-- <%# (Eval("zdjecie") != System.DBNull.Value ? "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) : "images/brak_foty.jpg") %> --%>
                            <%-- <asp:Image ID="imageControl" runat="server" Width="100" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileFilePath") != null ? Eval("FilePath") : Eval("PicURL")  %>'></asp:Image> --%>
                            <%--<dxe:aspxbinaryimage <%# ConvertOleObjectToByteArray(Eval("zdjecie")) %> ><img src="images/brak_foty.jpg" />
                        </dxe:aspxbinaryimage>--%>
                            <%--<img src='<%# string.Format("zdjecie", Convert.ToBase64String((byte[])Eval("zdjecie")))%>'/>--%>
                            <%--<%# byteArrayToImage(Eval("zdjecie")) %>--%>
                            <%--<%# ConvertOleObjectToByteArray(Eval("zdjecie")) %>--%>
                        </td>
                        <td>
                            <%# Eval("nazwa_produktu") %>
                        </td>
                        <td>
                            <%# Eval("ilosc_w_magazynie") %>
                        </td>
                        <td>
                            <%# Eval("cena_brutto") %>zł
                        </td>
                        <%--<td>
						<%# Eval("opis") %>
					</td>--%>
                        <td>
                            <asp:LinkButton align="left" runat="server" PostBackUrl='<%# String.Format("EdycjaProduktu1Szczegoly.aspx?{0}", Eval("id_produktu")) %>' Text="Edytuj produkt " />
                        </td>
                        <td></td>
                        
                    </ItemTemplate>
                </asp:ListView>


                <br />
                <br />
                <asp:Label ID="labelError" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Button ID="Button2" runat="server" Text="szczegóły" OnClick="Button2_Click" />
</asp:Content>