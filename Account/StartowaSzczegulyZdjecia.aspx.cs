﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class StartowaSzczegulyZdjecia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindListView();
            }
        }

        private void BindListView()
        {
            DataTable dataTable = new DataTable();

            string mojePolaczenie =
            "SERVER=localhost;" +
            "PORT=3306;" +
            "DATABASE=sklep;" +
            "UID=sylwek;" +
            "PASSWORD=sylwekl5";


            string sqlCommand = "SELECT id_fotografii, zdjecie, produkt FROM fotografia WHERE id_fotografii=" + Convert.ToInt32(this.ClientQueryString) + " AND zdjecie is not null;";

            try
            {
                using (var connection = new MySqlConnection(mojePolaczenie))
                {
                    connection.Open();

                    using (var command = new MySqlCommand(sqlCommand, connection))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }
            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }



            lvCustomers.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            lvCustomers.DataBind();
        }
    }
}