﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StartowaWyszukaj.aspx.cs" Inherits="sklepmaster.Account.StartowaWyszukaj" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style3 {
            width: 159px;
        }
        .table-produkty{
            border-radius: 16px;
            background-color: #caf5ff;
        }
        .table-produkty td{
            vertical-align: middle !important;/*important aby inne style nie nadpisywały*/
        }
        .table-produkty th{
            padding: 0px !important;
            background-color: #8adff2;
        }
        .table-produkty input{
            width: 100%;
            height: 84px;
            border:none;
            background-color: #8adff2;
        }
                .szukaj {
            padding: 6px 12px;
            font-size: 14px;
            font-weight: 400;
            line-height: 17px;
            color: #555;
            background-color: #51b9f3;
            text-align: left;
            border: 1px solid #feffff;
            border-radius: 4px;
            border-color: #000000;
            color: #fff;
        }
    </style>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1" >
        <tr>
            <td class="auto-style3" style="vertical-align:text-top;">
                <div class="input-group">
					<!--<asp:TextBox ID="TextBoxSzukaj" runat="server" class="form-control" placeholder="Szukaj" aria-describedby="ButtonSzukaj" style="width: 158px; top: 0px; left: 0px;" ></asp:TextBox>
                    <asp:Button ID="ButtonSzukaj" runat="server" Text="Szukaj" style="width: 158px;" OnClick="ButtonSzukaj_Click" />-->
				</div>
                <div>
                </div>

                <div>
                </div>
                <asp:TreeView ID="listaKategorii" runat="server" OnSelectedNodeChanged="listaKategorii_SelectedNodeChanged" ShowCheckBoxes="All" Width="217px" ImageSet="Arrows" ShowLines="True">
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                    <ParentNodeStyle Font-Bold="False" />
                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
                </asp:TreeView>
                <asp:Button ID="ButtonSzukajKategorie" CssClass="szukaj" runat="server" Text="Szukaj" OnClick="ButtonSzukajKategorie_Click" />
                <asp:Label ID="Labelbladszukajkategorie" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
            
            </td>
            <td>
                <div class="input_group">
                    <p align="center">
                        <asp:TextBox style="width: 200px;" class="input-group-addon" ID="TextBoxszukajj"  runat="server" ></asp:TextBox>
                        <asp:Button class="szukaj" ID="Button1" runat="server" Text="Szukaj" OnClick="Button1_Click1" />
                   
                        <asp:Label style="width: 100px;" ID="Labelszukaj" class="form-control" runat="server" Text=""></asp:Label>
                        </p>
                </div>        
                            


                <asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1"
                    ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging">
                    <LayoutTemplate>
                        <table border="0" style="width: 100%; height: inherit;" class="table table-produkty">
                            <tr style="border-top:none; background-color: #8adff2;">
                                <th style="border-top-left-radius: 16px;"></th>
                                <th>
                                    <asp:Button class="btn btn-default" ID="Button_Nazwa" runat="server"  Text="Nazwa" OnClick="Button_Nazwa_Click" />
                                </th>
                                <th><asp:Button class="btn btn-default" ID="Button1_ilosc_dostempna" runat="server"  Text="Ilość" OnClick="Button_Ilosc_Click" />
                                </th>
                                <th style=" display:flex; flex-direction:column;">
                                    <asp:Button style="height:42px;" class="btn btn-default" ID="Button_cena" runat="server"  Text="Cena ↑" OnClick="Button_Cena_Rosnaco_Click" />
                                    <asp:Button style="height:42px;" class="btn btn-default" ID="Button_cena_malejanco" runat="server"  Text="Cena ↓" OnClick="Button_Cena_Malejanco_Click" />
                                </th>
                                <th></th>
                                <th style="border-top-right-radius: 16px;"></th>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                            <tr>
                                <td colspan="3">
                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
                                        <Fields>
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                ShowNextPageButton="false" />
                                            <asp:NumericPagerField ButtonType="Link" />
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <tr>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                        </tr>
                    </GroupTemplate>
                    <ItemTemplate>
                        <td>
                            <asp:ImageButton Height="113px" Width="150px" runat="server" PostBackUrl='<%# String.Format("StartowaSzczeguly.aspx?{0}", Eval("id_produktu")) %>' ImageUrl='<%# Obraz(Eval("zdjecie"))  %> ' />
                        </td>
                        <td>
                            <%# Eval("nazwa_produktu") %>
                        </td>
                        <td>
                            <%# Eval("ilosc_w_magazynie") %>
                        </td>
                        <td>
                            <%# Eval("cena_brutto") %>zł
                        </td>
                        <%--<td>
						<%# Eval("opis") %>
					</td>--%>
                        <td>
                            <asp:LinkButton align="left" runat="server" PostBackUrl='<%# String.Format("StartowaSzczeguly.aspx?{0}", Eval("id_produktu")) %>' Text="szczegóły " />
                        </td>
                        <td style="width:100%;">
                            <asp:LinkButton runat="server" PostBackUrl='<%# String.Format("Koszyk.aspx?{0}", Eval("id_produktu")) %>' Text="dodaj do koszyka " />
                        </td>
                    </ItemTemplate>
                </asp:ListView>


                <br />
                <br />
                <asp:Label ID="labelError" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Button ID="Button2" runat="server" Text="szczegóły" OnClick="Button2_Click" />
</asp:Content>
