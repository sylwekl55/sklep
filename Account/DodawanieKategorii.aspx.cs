﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
	public partial class DodawanieKategorii : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				_WczytajKategorie();
			}
		}

		private List<WezelKategorii> _PobierzKategorie()
		{
			try
			{
				using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
					// Otwórz połączenie
					polaczenie.Open();

					return WezelKategorii.PobierzWszystkie(polaczenie);
				}
			}
			catch (Exception ex)
			{
				labelError.Text = "Błąd pobierania kategorii [" + ex.Message + "]";
				return null;
			}
		}

		protected void butDodaj_Click(object sender, EventArgs e)
		{
			TreeNode zaznaczonaKategoria = null; //inicjacja klasy drzewqa
			String nazwa = tbNazwaKategorii.Text;

            int zaznaczonakategoria, idzaznaczonejkategorii;

            sklepmaster.baza.RejestracjaKategorii r = new sklepmaster.baza.RejestracjaKategorii();

			if (listaKategorii.CheckedNodes.Count > 0)
			{
				zaznaczonaKategoria = listaKategorii.CheckedNodes[0];
			}

			labelError.Text = "Można dodać kategorie '" + nazwa + "' ";

			if (zaznaczonaKategoria == null)
			{
				labelError.Text += "jako kategorie główną";

                r.Nazwa = tbNazwaKategorii.Text;

                try
                {
                    if (!r.RejestrujKategorieBazowe())
                    {
                        labelBlad.Text = "Błąd dodawania kategorii: " + r.PowodNieZarejestrowania;
                        return;
                    }
                    labelBlad.Text = "Zarejestrowano ";
                    Response.Redirect(Request.Url.ToString()); // odświeża stronę 
                }
                catch (Exception ex)
                {
                    labelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
                    return;
                }
			}
			else
			{
				labelError.Text += "do kategorii " + zaznaczonaKategoria.Text + " o id: " + zaznaczonaKategoria.Value;

                if (!(Int32.TryParse(zaznaczonaKategoria.Value, out idzaznaczonejkategorii))) 
                {
                    labelBlad.Text = "nieprawidłowa kategoria";
                    return;
                }

                r.Bazowa = idzaznaczonejkategorii;
                r.Nazwa = tbNazwaKategorii.Text;

                try
                {
                    if(! r.RejestrujKategorie())
                    {
                        labelBlad.Text = "Błąd dodawania kategorii: " + r.PowodNieZarejestrowania;
                        return;
                    }
                    labelBlad.Text = "Zarejestrowano ";
                    Response.Redirect(Request.Url.ToString()); // odświeża stronę 
                }
                catch (Exception ex)
                {
                    labelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
                    return;
                }
			}
		}

		private void _WczytajKategorie()
		{
			List<WezelKategorii> kategorie = _PobierzKategorie();

			if (kategorie == null)
			{
				return;
			}

			//listaKategorii.DataSource = kategorie;
			foreach (WezelKategorii wezelBazowy in kategorie)
			{
				listaKategorii.Nodes.Add(wezelBazowy.TreeWezel);
			}

			listaKategorii.Attributes.Add("onclick", "client_OnTreeNodeChecked(event)");
			listaKategorii.ExpandAll();
		}

        protected void butUsun_Click(object sender, EventArgs e)
        {
            TreeNode zaznaczonaKategoria = null; //inicjacja klasy drzewqa
            String nazwa = tbNazwaKategorii.Text;

            if (listaKategorii.CheckedNodes.Count > 0)
            {
                zaznaczonaKategoria = listaKategorii.CheckedNodes[0];
            }

            labelError.Text = "Można usunąć kategorie '" + nazwa + "' ";

            int idzaznaczonejkategorii;
            sklepmaster.baza.UsunKategorie r = new sklepmaster.baza.UsunKategorie();

            if (!(Int32.TryParse(zaznaczonaKategoria.Value, out idzaznaczonejkategorii)))
            {
                labelBlad.Text = "nieprawidłowa kategoria";
                return;
            }

            r.Bazowa = idzaznaczonejkategorii;

            if (!r.UsuwanieKategorii())
            {
                labelBlad.Text = "Błąd usunięcia kategorii: " + r.PowodNieUdanegoUsuniecia;
                return;
            }
            labelBlad.Text = "Zarejestrowano ";
            Response.Redirect(Request.Url.ToString()); // odświeża stronę 
            
            
        }

        protected void butEdytuj_Click(object sender, EventArgs e)
        {
            TreeNode zaznaczonaKategoria = null; //inicjacja klasy drzewqa
            String nazwa = tbNazwaKategorii.Text;

            if (listaKategorii.CheckedNodes.Count > 0)
            {
                zaznaczonaKategoria = listaKategorii.CheckedNodes[0];
            }

            labelError.Text = "Można edytować kategorie '" + nazwa + "' ";

            int idzaznaczonejkategorii;
            sklepmaster.baza.EdytujKategorie r = new sklepmaster.baza.EdytujKategorie();

            if (!(Int32.TryParse(zaznaczonaKategoria.Value, out idzaznaczonejkategorii)))
            {
                labelBlad.Text = "nieprawidłowa kategoria";
                return;
            }

            r.Bazowa = idzaznaczonejkategorii;
            r.Nazwa = tbNazwaKategorii.Text;

            if (!r.EdytownieKategorii())
            {
                labelBlad.Text = "Błąd zmienić kategorie: " + r.PowodNieUdanejEdycji;
                return;
            }
            labelBlad.Text = "Edytowano ";
            Response.Redirect(Request.Url.ToString()); // odświeża stronę 
        }
	}
}