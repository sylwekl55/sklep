﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Zamowienie.aspx.cs" Inherits="sklepmaster.Account.Zamowienie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style3 {
            width: 159px;
        }
        .table-produkty{
            border-radius: 16px;
            background-color: #caf5ff;
        }
        .table-produkty td{
            vertical-align: middle !important;/*important aby inne style nie nadpisywały*/
        }
        .table-produkty th{
            padding: 0px !important;
            background-color: #8adff2;
        }
        .table-produkty input{
            width: 100%;
            height: 84px;
            border:none;
            background-color: #8adff2;
        }
                 .zielony {
                     background: #68f11a
            
        }
                 .czerwony {
                     background: #fa4747
            
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1" >
        <tr>
            
            <td>
                <asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1"
                    ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging">
                    <LayoutTemplate>
                        <table border="0" style="width: 100%; height: inherit;" class="table table-produkty">
                            <tr style="border-top:none; background-color: #8adff2;">
                               <%-- <th style="border-top-left-radius: 16px;"></th>
                                <th>
                                    <asp:Button class="btn btn-default" ID="Button_Nazwa" runat="server"  Text="Nazwa" OnClick="Button_Nazwa_Click" />
                                </th>
                                <th><asp:Button class="btn btn-default" ID="Button1_ilosc_dostempna" runat="server"  Text="Ilość" OnClick="Button_Ilosc_Click" />
                                </th>
                                <th style=" display:flex; flex-direction:column;">
                                    <asp:Button style="height:42px;" class="btn btn-default" ID="Button_cena" runat="server"  Text="Cena ↑" OnClick="Button_Cena_Rosnaco_Click" />
                                    <asp:Button style="height:42px;" class="btn btn-default" ID="Button_cena_malejanco" runat="server"  Text="Cena ↓" OnClick="Button_Cena_Malejanco_Click" />
                                </th>
                                <th></th>
                                <th style="border-top-right-radius: 16px;"></th>--%>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                            <tr>
                                <td colspan="3">
                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
                                        <Fields>
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                ShowNextPageButton="false" />
                                            <asp:NumericPagerField ButtonType="Link" />
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <tr>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                        </tr>
                    </GroupTemplate>
                    <ItemTemplate >
                        <td>
                            <%--<asp:ImageButton Height="113px" Width="150px" runat="server" PostBackUrl='<%# String.Format("StartowaSzczeguly.aspx?{0}", Eval("id_produktu")) %>' ImageUrl='<%# Obraz(Eval("zdjecie"))  %> ' />
                            <%-- <%# (Eval("zdjecie") != System.DBNull.Value ? "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) : "images/brak_foty.jpg") %> --%>
                            <%-- <asp:Image ID="imageControl" runat="server" Width="100" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileFilePath") != null ? Eval("FilePath") : Eval("PicURL")  %>'></asp:Image> --%>
                            <%--<dxe:aspxbinaryimage <%# ConvertOleObjectToByteArray(Eval("zdjecie")) %> ><img src="images/brak_foty.jpg" />
                        </dxe:aspxbinaryimage>--%>
                            <%--<img src='<%# string.Format("zdjecie", Convert.ToBase64String((byte[])Eval("zdjecie")))%>'/>--%>
                            <%--<%# byteArrayToImage(Eval("zdjecie")) %>--%>
                            <%--<%# ConvertOleObjectToByteArray(Eval("zdjecie")) %>--%>
                        </td>
                        
                        <td style="width:25%;" class=<%# (string.IsNullOrEmpty(Convert.ToString(Eval("data_zrealizowania_zamowienia"))) ? "czerwony" : "zielony") %> >
                            
                            <a > <%# Eval("data_zamowienia") %> </a>
                        </td>
                        <td >
                            <%# Eval("sposob_platnosci") %>
                        </td>
                        <%--<td>
						<%# Eval("opis") %>
					</td>--%>
                        <td>
                            <asp:LinkButton align="left" runat="server" PostBackUrl='<%# String.Format("ZamowienieSzczegoly.aspx?{0}", Eval("id_zamowienie")) %>' Text="szczegóły " />
                        </td>
                        <td style="width:100%;">
                             <%--<asp:LinkButton runat="server" OnCommand="LinkButton_Command" PostBackUrl='<%# String.Format("Koszyk.aspx?{0}", Eval("id_produktu")) %>' Text="dodaj do koszyka " />
                           <%-- <asp:LinkButton runat="server" OnCommand="LinkButton_Command" CommandArgument='<%#Eval("id_produktu") %>' Text="dodaj do koszyka " />
                           <%-- <asp:Button class="btn btn-default btn-primary" ID="Button"  CommandArgument='<%#Eval("id_produktu") %>' runat="server" Text="dodaj" OnClientClick="aa" />
                        --%></td>
                    </ItemTemplate>
                </asp:ListView>


                <br />
                <br />
                <asp:Label ID="labelError" runat="server" Text="Label" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>

    </asp:Content>