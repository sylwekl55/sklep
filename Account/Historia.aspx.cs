﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class Historia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int idOsoba = Convert.ToInt32(Session["IdOsoba"].ToString());
            int idklient = Convert.ToInt32(Session["IdKlient"].ToString());

            BindListView();

        }
        private void BindListView()
        {
            int idOsoba = Convert.ToInt32(Session["IdOsoba"].ToString());
            int idklient = Convert.ToInt32(Session["IdKlient"].ToString());

            DataTable dataTable = new DataTable();

            string sqlCommand = "SELECT id_zamowienie, data_zrealizowania_zamowienia, data_zamowienia, sposob_platnosci, zaplacono, adres, klient from zamowienie where klient=" + idklient + " order by  data_zamowienia desc;";


            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();
                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }


            lvCustomers.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            lvCustomers.DataBind();

            //DropDownList1.DataTextField = "vat";

            // pole ukryte
            //DropDownList1.DataValueField = "id_vat";

            // źródło danych 
            //DropDownList1.DataSource = dataTable;

            //DropDownList1.DataBind();
        }

        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (lvCustomers.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.BindListView();
        }
    }
}