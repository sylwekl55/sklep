﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class ZamowienieSzczegoly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindListView();
                this.BindListView1();
                //this.BindListView2();

            }
        }

        private void BindListView()
        {
            DataTable dataTable = new DataTable();

            //string sqlCommand = "SELECT id_fotografii, zdjecie FROM fotografia WHERE produkt=" + Convert.ToInt32(this.ClientQueryString) + " AND zdjecie is not null;";
            string sqlCommand = "Select * from zamowienie, adres, osoba where zamowienie.adres=adres.id_adresu AND id_zamowienie=" + Convert.ToInt32(this.ClientQueryString) + " AND osoba.adres=adres.id_adresu;";
            
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }
            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }



            lvCustomers.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            lvCustomers.DataBind();
        }
        private void BindListView1()
        {
            DataTable dataTable = new DataTable();
            string sqlCommand = "Select nazwa_produktu, cena_brutto, ilosc from element_zamowienia, Produkt where zamowienie=" + Convert.ToInt32(this.ClientQueryString) + " AND Produkt.id_produktu = element_zamowienia.produkt";

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }
            foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
            {
                // ... Write value of first field as integer.
                //Console.WriteLine(row.Field<int>(0));
            }



            ListView1.DataSource = dataTable;
            //foreach (var asd in lvCustomers.DataSource)
            //{
            ////	asd.
            //}
            ListView1.DataBind();

        }

        protected void ZrealizowacZamowienie_Click(object sender, EventArgs e)
        {
            sklepmaster.baza.pola.Kup kup = new sklepmaster.baza.pola.Kup();
            kup.id_zamowienie = Convert.ToInt32(this.ClientQueryString);
            kup.Edytuj();
            Page.Response.Redirect("/Account/Zamowienie.aspx");
        }
    }
}