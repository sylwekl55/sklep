﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace sklepmaster.Account
{
    public partial class DodawanieProduktu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillDropDownList();
            }
        }
		protected void ButtonDodaj_Click(object sender, EventArgs e)
		{
            sklepmaster.baza.pola.RejestracjaProduktu rej = new sklepmaster.baza.pola.RejestracjaProduktu();
            
            int ilossc_w_magazynie, producent, vat, promocje, kategoria;
            float cena_brutto;

            if(!(Int32.TryParse(TextBoxIloscWMagazynie.Text, out ilossc_w_magazynie)))
            {
                LabelBlad.Text = "nie prawidłowa ilość w magazynie ";
                return;
            }
            if (!(float.TryParse(TextBoxCena.Text, out cena_brutto)))
            {
                LabelBlad.Text = "nie prawidłowa cena ";
                return;
            }
            if (!(Int32.TryParse(DropDownListProducent.SelectedValue, out producent)))
            {
                LabelBlad.Text = "nie wybrno odpowiedniego producenta ";
                return;
            }
            if (!(Int32.TryParse(DropDownListVat.SelectedValue, out vat)))
            {
                LabelBlad.Text = "nie wybrno odpowiedniego vatu ";
                return;
            }
            if (!(Int32.TryParse(DropDownListPromocja.SelectedValue, out promocje)))
            {
                LabelBlad.Text = "nie wybrno odpowiedniego vatu ";
                return;
            }
            if (!(Int32.TryParse(DropDownListKategoria.SelectedValue, out kategoria)))
            {
                LabelBlad.Text = "nie wybrno odpowiedniego vatu ";
                return;
            }
            //Label1.Text = DropDownListProducent.DataValueField.ToString();
            //string a = DropDownListProducent.DataValueField.ToString(), b = DropDownListVat.DataValueField.ToString(), c = DropDownListPromocja.DataValueField.ToString(), d = DropDownListKategoria.DataValueField.ToString();
            //string ea = "spadaj";
            //int producent1 = Convert.ToInt32(DropDownListProducent.SelectedValue);
            //int vat1 = Convert.ToInt32(DropDownListVat.SelectedValue);
            //int promocje1 = Convert.ToInt32(c);
            //int kategoria1 = Convert.ToInt32(d);
            
            rej.NazwaProduktu = TextBoxNazwaProduktu.Text;
            rej.IloscwMagazynie = ilossc_w_magazynie;
            rej.Wersja = TextBoxWersja.Text;
            rej.Opis = TextBoxOpis.Text;
            rej.CenaBrutto = cena_brutto;
            rej.Producent = producent;
            rej.Vat = vat;
            rej.Promocje = promocje;
            rej.Kategoria = kategoria;


            try
            {
                if (!rej.Rejestruj())
                {
                    LabelBlad.Text = "Błąd dodawania klienta: " + rej.PowodNieZarejestrowania;
                    return;
                }

                LabelBlad.Text = "Zarejestrowano ";
            }
            catch (Exception ex)
            {
                LabelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
                return;
            }
		}


        private void fillDropDownList()
        {
            DataTable dataTable = new DataTable();
            DataTable dataTable1 = new DataTable();
            DataTable dataTableKategoria = new DataTable();
            DataTable dataTablePromocja = new DataTable();

            string sqlCommand = "SELECT id_vat, vat from vat";
            string sqlCommand1 = "SELECT id_producent, nazwa_firmy, nip, regon from Producent order by nazwa_firmy";
            string sqlCommandKategoria = "SELECT id_kategoria, nazwa from kategoria";
            string sqlCommandPromocja = "SELECT id_promocji, nazwa_promocji, data_zaczecia_promocji, data_zakonczenia_promocji from promocje";

            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }

                    using (var command = new MySqlCommand(sqlCommand1, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTable1);
                        }
                    }

                    using (var command = new MySqlCommand(sqlCommandKategoria, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTableKategoria);
                        }
                    }


                    using (var command = new MySqlCommand(sqlCommandPromocja, polaczenie))
                    {
                        using (var adapter = new MySqlDataAdapter(command))
                        {
                            adapter.Fill(dataTablePromocja);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading data table [" + ex.Message + "]", ex);
            }

            // pole wyświetlane
            DropDownListVat.DataTextField = "vat";
            // pole ukryte
            DropDownListVat.DataValueField = "id_vat";
            // źródło danych 
            DropDownListVat.DataSource = dataTable;
            DropDownListVat.DataBind();



            // pole wyświetlane w dropdown lsist działa 
            dataTable1.Columns.Add(new DataColumn("Producent", System.Type.GetType("System.String"), "nazwa_firmy + ' nip: ' + nip + ' regon: ' + regon"));
            DropDownListProducent.DataTextField = "Producent";

            // pole ukryte
            DropDownListProducent.DataValueField = "id_producent";

            // źródło danych 
            DropDownListProducent.DataSource = dataTable1;
            //DropDownListProdycent.DataTextFormatString = "{0} - {1}";
            //DropDownListProdycent.DataSource = "id_producent,nazwa_firmy";

            DropDownListProducent.DataBind();





            // pole wyświetlane kategorii
            DropDownListKategoria.DataTextField = "nazwa";
            // pole ukryte
            DropDownListKategoria.DataValueField = "id_kategoria";
            // źródło danych 
            DropDownListKategoria.DataSource = dataTableKategoria;
            DropDownListKategoria.DataBind();


            // pole wyświetlane promocjii
            dataTablePromocja.Columns.Add(new DataColumn("Promocjee", System.Type.GetType("System.String"), "nazwa_promocji + ' rozpoczęcie: ' + data_zaczecia_promocji + ' zakończenie: ' + data_zakonczenia_promocji"));

            DropDownListPromocja.DataTextField = "Promocjee";
            // pole ukryte
            DropDownListPromocja.DataValueField = "id_promocji";
            // źródło danych 
            DropDownListPromocja.DataSource = dataTablePromocja;
            DropDownListPromocja.DataBind();
        }

        protected void buttonDodajProducenta_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/DodajProducenta.aspx");
        }

        
    }
}