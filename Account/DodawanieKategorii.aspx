﻿<%@ Page 
	
	Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DodawanieKategorii.aspx.cs" Inherits="sklepmaster.Account.DodawanieKategorii" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		.auto-style1 {
			width: 100%;
		}
		.auto-style2 {
			height: 25px;
		}
		.auto-style3 {
			height: 15px;
		}
		.auto-style4 {
			width: 236px;
		}
		.auto-style5 {
			height: 15px;
			width: 236px;
		}
        .tabele {
            border-radius: 16px !important;
            border="0" ;
            border-spacing: 0mm 2mm;
        }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<script type="text/javascript">
		function client_OnTreeNodeChecked(event)
		{
			var TreeNode = event.srcElement || event.target;

			if (TreeNode.tagName == "INPUT" && TreeNode.type == "checkbox")
			{
				if (TreeNode.checked)
				{
					uncheckOthers(TreeNode.id);
				}
			}
		}

		function uncheckOthers(id)
		{
			var elements = document.getElementsByTagName('input');

			// loop through all input elements in form
			for (var i = 0; i < elements.length; i++)
			{
				if (elements.item(i).type == "checkbox")
				{
					if (elements.item(i).id != id)
					{
						elements.item(i).checked = false;
					}
				}
			}
		}
	</script>
	<table class=" tabele auto-style1">
		<tr>
			<td class="auto-style4">
				<asp:Label ID="Label1" runat="server" Text="Wybierz kategorie nadrzędną"></asp:Label>
			</td>
			<td>
				<asp:TreeView  ID="listaKategorii" runat="server" 
					ShowCheckBoxes="All">
				</asp:TreeView>
			</td>
		</tr>
		<tr>
			<td class="auto-style4">
				<asp:Label ID="Label2" runat="server" Text="Nazwa nowej kategorii"></asp:Label>
			</td>
			<td>
				<asp:TextBox ID="tbNazwaKategorii" runat="server" Width="588px"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td class="auto-style4">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style4">
				<asp:Button ID="butDodaj" runat="server" Text="Dodaj" OnClick="butDodaj_Click"/>
			</td>

			<td class="auto-style4">
            <asp:Button ID="butUsun" runat="server" Text="Usuń" OnClick="butUsun_Click"/>
            </td>
		</tr>
		<tr>
			<td class="auto-style4">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style4">
            <asp:Button ID="butEdytuj" runat="server" Text="Edytuj nazwę Kategorii" OnClick="butEdytuj_Click"/>
            </td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style4">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style5">
				<asp:Label ID="labelError" runat="server" Text=""></asp:Label>
			</td>
			<td class="auto-style3">
                <asp:Label ID="labelBlad" runat="server"></asp:Label>
            </td>
		</tr>
	</table>
</asp:Content>
