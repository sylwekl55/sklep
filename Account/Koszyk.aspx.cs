﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class Koszyk : System.Web.UI.Page
    {
        public int iloscmax, sumamax, dentysta = 3;
        public float cena;
        public string nazwa;
       

        List<sklepmaster.baza.pola.KoszykPola> koszyk = new List<baza.pola.KoszykPola>();//lista obiektów moje dane 
        protected void Page_Load(object sender, EventArgs e)
        {
            //LabelBland.Text = this.ClientQueryString; //przesyłamy do koszyka element
            Ciasteczka(this.ClientQueryString); // dodaje do koszyka przedmiot czyli id 
            WczytajCiastka();
            //Suma();

            sklepmaster.baza.pola.KoszykPola koszyk1 = new baza.pola.KoszykPola();

            //koszyk.Add(new baza.pola.KoszykPola { Id_produktu = Convert.ToInt32(this.ClientQueryString) }); // przekazanie do listy koszyk elementu

            // --------------------- wczytanie listy
            foreach(var i in koszyk)
            {
               // LabelBland.Text ="  !!!" + i.Id_produktu.ToString() + "     ";
            }

           // LabelBland.Text = Server.HtmlEncode(Request.Cookies["ciasteczko1"].Value);
            List<String> a = new List<String>();

            HttpCookie ciasteczko = new HttpCookie("ciasteczka");

            //Response.Cookies.Add(new HttpCookie());

            //Response.Cookies ["a"].Value = this.ClientQueryString;
            Response.Cookies[this.ClientQueryString].Value = this.ClientQueryString;

            //Pętla po elementach ssji
            //int j=0, ii=0;

           // j = Session.Contents.Count;
            //for (ii=1; ii<=j;ii++)
            //{
                //string aaa = Server.HtmlEncode(Request.Cookies[Convert.ToString(ii)].Value);
           // }
            
            //Response.Cookies["ciasteczko1"].Value = null;
            
    
        }

        
        private void WczytajCiastka()
        {

            if (Request.Cookies["ciasteczko1"] != null)//sprawdzamy cisteczka czy są puste 
            {
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko1"].Value));
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc1"].Value));
                LabelIlosc1.Text = Convert.ToString(aa);
                Wczytaj(a);
                LabelNazwa1.Text = nazwa;
                LabelCena.Text = Convert.ToString(cena);
                LabelIloscMax1.Text =Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc1.Visible = false;
                ButtonAktualizuj1.Visible = false;
                LabelNazwa1.Visible = false;
                LabelCena.Visible = false;
                LabelIloscMax1.Visible = false;
                TextBoxIlosc1.Visible = false;
                ButtonUsuń1.Visible = false;
                Label_1.Visible = false;
            }
            if (Request.Cookies["ciasteczko2"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc2"].Value));
                LabelIlosc2.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko2"].Value));
                Wczytaj(a);
                LabelNazwa2.Text = nazwa;
                LabelCena2.Text = Convert.ToString(cena);
                LabelIloscMax2.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc2.Visible = false;
                ButtonAktualizuj2.Visible = false;
                LabelNazwa2.Visible = false;
                LabelCena2.Visible = false;
                LabelIloscMax2.Visible = false;
                TextBoxIlosc2.Visible = false;
                ButtonUsun2.Visible = false;
                Label_2.Visible = false;
            }
            if (Request.Cookies["ciasteczko3"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc3"].Value));
                LabelIlosc3.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko3"].Value));
                Wczytaj(a);
                LabelNazwa3.Text = nazwa;
                LabelCena3.Text = Convert.ToString(cena);
                LabelIloscMax3.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc3.Visible = false;
                ButtonAktualizuj3.Visible = false;
                LabelNazwa3.Visible = false;
                LabelCena3.Visible = false;
                LabelIloscMax3.Visible = false;
                TextBoxIlosc3.Visible = false;
                ButtonUsun3.Visible = false;
                Label_3.Visible = false;
            }
            if (Request.Cookies["ciasteczko4"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc4"].Value));
                LabelIlosc4.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko4"].Value));
                Wczytaj(a);
                LabelNazwa4.Text = nazwa;
                LabelCena4.Text = Convert.ToString(cena);
                LabelIloscMax4.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc4.Visible = false;
                ButtonAktualizuj4.Visible = false;
                LabelNazwa4.Visible = false;
                LabelCena4.Visible = false;
                LabelIloscMax4.Visible = false;
                TextBoxIlosc4.Visible = false;
                ButtonUsun4.Visible = false;
                Label_4.Visible = false;
            }
            if (Request.Cookies["ciasteczko5"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc5"].Value));
                LabelIlosc5.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko5"].Value));
                Wczytaj(a);
                LabelNazwa5.Text = nazwa;
                LabelCena5.Text = Convert.ToString(cena);
                LabelIloscMax5.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc5.Visible = false;
                ButtonAktualizuj5.Visible = false;
                LabelNazwa5.Visible = false;
                LabelCena5.Visible = false;
                LabelIloscMax5.Visible = false;
                TextBoxIlosc5.Visible = false;
                ButtonUsun5.Visible = false;
                Label_5.Visible = false;
            }
            if (Request.Cookies["ciasteczko6"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc6"].Value));
                LabelIlosc6.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko6"].Value));
                Wczytaj(a);
                LabelNazwa6.Text = nazwa;
                LabelCena6.Text = Convert.ToString(cena);
                LabelIloscMax6.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc6.Visible = false;
                ButtonAktualizuj6.Visible = false;
                LabelNazwa6.Visible = false;
                LabelCena6.Visible = false;
                LabelIloscMax6.Visible = false;
                TextBoxIlosc6.Visible = false;
                ButtonUsun6.Visible = false;
                Label_6.Visible = false;
            }
            if (Request.Cookies["ciasteczko7"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc7"].Value));
                LabelIlosc7.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko7"].Value));
                Wczytaj(a);
                LabelNazwa7.Text = nazwa;
                LabelCena7.Text = Convert.ToString(cena);
                LabelIloscMax7.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc7.Visible = false;
                ButtonAktualizuj7.Visible = false;
                LabelNazwa7.Visible = false;
                LabelCena7.Visible = false;
                LabelIloscMax7.Visible = false;
                TextBoxIlosc7.Visible = false;
                ButtonUsun7.Visible = false;
                Label_7.Visible = false;
            }
            if (Request.Cookies["ciasteczko8"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc8"].Value));
                LabelIlosc8.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko8"].Value));
                Wczytaj(a);
                LabelNazwa8.Text = nazwa;
                LabelCena8.Text = Convert.ToString(cena);
                LabelIloscMax8.Text =Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc8.Visible = false;
                ButtonAktualizuj8.Visible = false;
                LabelNazwa8.Visible = false;
                LabelCena8.Visible = false;
                LabelIloscMax8.Visible = false;
                TextBoxIlosc8.Visible = false;
                ButtonUsun8.Visible = false;
                Label_8.Visible = false;
            }
            if (Request.Cookies["ciasteczko9"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc9"].Value));
                LabelIlosc9.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko9"].Value));
                Wczytaj(a);
                LabelNazwa9.Text = nazwa;
                LabelCena9.Text = Convert.ToString(cena);
                LabelIloscMax9.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc9.Visible = false;
                ButtonAktualizuj9.Visible = false;
                LabelNazwa9.Visible = false;
                LabelCena9.Visible = false;
                LabelIloscMax9.Visible = false;
                TextBoxIlosc9.Visible = false;
                ButtonUsun9.Visible = false;
                Label_9.Visible = false;
            }
            if (Request.Cookies["ciasteczko10"] != null)
            {
                int aa = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ilosc10"].Value));
                LabelIlosc10.Text = Convert.ToString(aa);
                int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko10"].Value));
                Wczytaj(a);
                LabelNazwa10.Text = nazwa;
                LabelCena10.Text = Convert.ToString(cena);
                LabelIloscMax10.Text = Convert.ToString(iloscmax);
            }
            else
            {
                LabelIlosc10.Visible = false;
                ButtonAktualizuj10.Visible = false;
                LabelNazwa10.Visible = false;
                LabelCena10.Visible = false;
                LabelIloscMax10.Visible = false;
                TextBoxIlosc10.Visible = false;
                ButtonUsun10.Visible = false;
                Label_10.Visible = false;
            }
        }

        public void Wczytaj(int a)
        {
                
                try
                {
                    using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                    {
                        // Otwórz połączenie
                        polaczenie.Open();

                        String Polonczenieciasteczko1 = "SELECT " + sklepmaster.baza.pola.Produkt.NazwaProduktu + ',' + sklepmaster.baza.pola.Produkt.IloscWMagazynie + ',' + sklepmaster.baza.pola.Produkt.CenaBrutto +
                        " FROM " + sklepmaster.baza.pola.Produkt.NazwaTabeli + " WHERE " + sklepmaster.baza.pola.Produkt.IdProduktu + "=" + sklepmaster.baza.pola.Produkt.ParamIdProduktu;

                        MySqlDataAdapter adapter = new MySqlDataAdapter(Polonczenieciasteczko1, polaczenie);
                        adapter.SelectCommand.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamIdProduktu, a);

                        System.Data.DataTable tabela = new System.Data.DataTable();
                        adapter.Fill(tabela);
                        System.Data.DataRow rekord = tabela.Rows[0];

                        sklepmaster.baza.DaneProduktu dane = new sklepmaster.baza.DaneProduktu();
                        nazwa = (String)rekord[sklepmaster.baza.pola.Produkt.NazwaProduktu];
                        cena= (float)rekord[sklepmaster.baza.pola.Produkt.CenaBrutto];
                        iloscmax= (int)rekord[sklepmaster.baza.pola.Produkt.IloscWMagazynie];
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        public void Ciasteczka(string p)
        {
            int convert = Convert.ToInt32(this.ClientQueryString);
            //int convert = Convert.ToInt32(p);

            if (Request.Cookies["ciasteczko1"] == null)//sprawdzamy cisteczka czy są puste 
            {
                Response.Cookies["ciasteczko1"].Value = this.ClientQueryString;//przypożądkownaie dla ciasteczka zmiennej
                Response.Cookies["ilosc1"].Value = "1";//przyporządkowanie dla ciasteczka zmiennej
                return;
            }
            int ciastko1 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko1"].Value));
            if ( ciastko1 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko2"] == null)
            {
                Response.Cookies["ciasteczko2"].Value = this.ClientQueryString;
                Response.Cookies["ilosc2"].Value = "1";
                return;
            }
            int ciasteczko2 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko2"].Value));
            if (ciasteczko2 == convert)
            {
                Response.Cookies["ilosc2"].Value = "1";
                return;
            }
            if (Request.Cookies["ciasteczko3"] == null)
            {
                Response.Cookies["ciasteczko3"].Value = this.ClientQueryString;
                Response.Cookies["ilosc3"].Value = "1";

                return;
            }
            int ciasteczko3 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko3"].Value));
            if (ciasteczko3 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko4"] == null)
            {
                Response.Cookies["ciasteczko4"].Value = this.ClientQueryString;
                Response.Cookies["ilosc4"].Value = "1";

                return;
            }
            int ciasteczko4 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko4"].Value));
            if (ciasteczko4 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko5"] == null)
            {
                Response.Cookies["ciasteczko5"].Value = this.ClientQueryString;
                Response.Cookies["ilosc5"].Value = "1";

                return;
            }
            int ciasteczko5 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko5"].Value));
            if (ciasteczko5 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko6"] == null)
            {
                Response.Cookies["ciasteczko6"].Value = this.ClientQueryString;
                Response.Cookies["ilosc6"].Value = "1";

                return;
            }
            int ciasteczko6 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko6"].Value));
            if (ciasteczko6 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko7"] == null)
            {
                Response.Cookies["ciasteczko7"].Value = this.ClientQueryString;
                Response.Cookies["ilosc7"].Value = "1";

                return;
            }
            int ciasteczko7 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko7"].Value));
            if (ciasteczko7 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko8"] == null)
            {
                Response.Cookies["ciasteczko8"].Value = this.ClientQueryString;
                Response.Cookies["ilosc8"].Value = "1";

                return;
            }
            int ciasteczko8 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko8"].Value));
            if (ciasteczko8 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko9"] == null)
            {
                Response.Cookies["ciasteczko9"].Value = this.ClientQueryString;
                Response.Cookies["ilosc9"].Value = "1";

                return;
            }
            int ciasteczko9 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko9"].Value));
            if (ciasteczko9 == convert)
            {
                return;
            }
            if (Request.Cookies["ciasteczko10"] == null)
            {
                Response.Cookies["ciasteczko10"].Value = this.ClientQueryString;
                Response.Cookies["ilosc10"].Value = "1";

                return;
            }
            int ciasteczko10 = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko10"].Value));
            if (ciasteczko10 == convert)
            {
                return;
            }
        }

        protected void ButtonUsuń_Click(object sender, EventArgs e)
        {
            Usun();
        }

        protected void ButtonUsuń1_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko1"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun2_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko2"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun3_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko3"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun4_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko4"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun5_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko5"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun6_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko6"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun7_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko7"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun8_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko8"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun9_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko9"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonUsun10_Click(object sender, EventArgs e)
        {
            Response.Cookies["ciasteczko10"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonKup_Click(object sender, EventArgs e)
        {
            if (sklepmaster.baza.DaneZalogowanejOsoby.CzyZalogowany(Session["klient"]))
            {
                sklepmaster.baza.pola.Kup ku = new sklepmaster.baza.pola.Kup();
                if (Request.Cookies["ciasteczko1"] != null)
                {
                    int aa = Convert.ToInt32(LabelIloscMax1.Text);
                    if (Convert.ToInt64(TextBoxIlosc1.Text) > aa)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa1.Text + "   " + aa;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko2"] != null)
                {
                    int aaa = Convert.ToInt32(LabelIloscMax2.Text);
                    if (Convert.ToInt32(TextBoxIlosc2.Text) > aaa)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa2.Text + "   " + aaa;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko3"] != null)
                {
                    int ab = Convert.ToInt32(LabelIloscMax3.Text);
                    if (Convert.ToInt32(TextBoxIlosc2.Text) > ab)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa3.Text + "   " + ab;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko4"] != null)
                {
                    int ac = Convert.ToInt32(LabelIloscMax4.Text);
                    if (Convert.ToInt32(TextBoxIlosc4.Text) > ac)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa4.Text + "   " + ac;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko5"] != null)
                {
                    int ad = Convert.ToInt32(LabelIloscMax5.Text);
                    if (Convert.ToInt32(TextBoxIlosc5.Text) > ad)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa5.Text + "   " + ad;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko6"] != null)
                {
                    int af = Convert.ToInt32(LabelIloscMax6.Text);
                    if (Convert.ToInt32(TextBoxIlosc6.Text) > af)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa6.Text + "   " + af;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko7"] != null)
                {
                    int ag = Convert.ToInt32(LabelIloscMax7.Text);
                    if (Convert.ToInt32(TextBoxIlosc7.Text) > ag)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa7.Text + "   " + ag;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko8"] != null)
                {
                    int ah = Convert.ToInt32(LabelIloscMax8.Text);
                    if (Convert.ToInt32(TextBoxIlosc8.Text) > ah)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa8.Text + "   " + ah;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko9"] != null)
                {
                    int aj = Convert.ToInt32(LabelIloscMax9.Text);
                    if (Convert.ToInt32(TextBoxIlosc9.Text) > aj)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa9.Text + "   " + aj;
                        return;
                    }
                }
                if (Request.Cookies["ciasteczko10"] != null)
                {
                    int ak = Convert.ToInt32(LabelIloscMax10.Text);
                    if (Convert.ToInt32(TextBoxIlosc10.Text) > ak)
                    {
                        LabelBland.Text = "Ilość dostępna produktów w magazynie: " + LabelNazwa10.Text + "   " + ak;
                        return;
                    }
                }





                if (Request.Cookies["ciasteczko1"] != null)//sprawdzamy cisteczka czy są puste 
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko1"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc1.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax1.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko2"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko2"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc2.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax2.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko3"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko3"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc3.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax3.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko4"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko4"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc4.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax4.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko5"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko5"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc5.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax5.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko6"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko6"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc6.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax6.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko7"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko7"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc7.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax7.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko8"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko8"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc8.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax8.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko9"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko9"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc9.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax9.Text);
                    ku.Odejmij();
                }
                if (Request.Cookies["ciasteczko10"] != null)
                {
                    int a = Convert.ToInt32(Server.HtmlEncode(Request.Cookies["ciasteczko10"].Value));
                    ku.id_produktu = a;
                    ku.ilosc_odjeta = Convert.ToInt32(TextBoxIlosc10.Text);
                    ku.ilosc_w_magazynie = Convert.ToInt32(LabelIloscMax10.Text);
                    ku.Odejmij();
                }

                Usun();
                Page.Response.Redirect("/Account/Koszyk1.aspx");
            }
            else
            {
                Page.Response.BufferOutput = true;
                Page.Response.Redirect("/Account/Logowanie.aspx");
            }

        }

        private void Usun()
        {
            Response.Cookies["ciasteczko1"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko2"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko3"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko4"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko5"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko6"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko7"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko8"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko9"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ciasteczko10"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc1"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc2"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc3"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc4"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc5"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc6"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc7"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc8"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc9"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["ilosc10"].Expires = DateTime.Now.AddDays(-1);
            Page.Response.Redirect("/Account//Starowa.aspx");
        }

        protected void ButtonSuma_Click(object sender, EventArgs e)
        {
            int ilosc1 = 0, ilosc2 = 0, ilosc3 = 0, ilosc4 = 0, ilosc5 = 0, ilosc6 = 0, ilosc7 = 0, ilosc8 = 0, ilosc9 = 0, ilosc10 = 0;
            Single cena1 = 0, cena2 = 0, cena3 = 0, cena4 = 0, cena5 = 0, cena6 = 0, cena7 = 0, cena8 = 0, cena9 = 0, cena10 = 0;
            Single suma, suma1=0, suma2=0, suma3=0, suma4=0, suma5=0, suma6=0, suma7=0, suma8=0, suma9=0, suma10=0;
            if (Request.Cookies["ciasteczko1"] != null)
            {
                cena1 = Convert.ToSingle(LabelCena.Text);
                ilosc1 = Convert.ToInt32(LabelIlosc1.Text);
                suma1 =cena1 * ilosc1;
            }
            if (Request.Cookies["ciasteczko2"] != null)
            {
                cena2 = Convert.ToSingle(LabelCena2.Text);
                ilosc2 = Convert.ToInt32(LabelIlosc2.Text);
                suma2 = cena2 * ilosc2;
            }
            if (Request.Cookies["ciasteczko3"] != null)
            {
                cena3 = Convert.ToSingle(LabelIlosc3.Text);
                ilosc3 = Convert.ToInt32(TextBoxIlosc3.Text);
                suma3 = cena3 * ilosc3;
            }
            if (Request.Cookies["ciasteczko4"] != null)
            {
                cena4 = Convert.ToSingle(LabelIlosc4.Text);
                ilosc4 = Convert.ToInt32(TextBoxIlosc4.Text);
                suma4 = cena4 * ilosc4;
            }
            if (Request.Cookies["ciasteczko5"] != null)
            {
                cena5 = Convert.ToSingle(LabelIlosc5.Text);
                ilosc5 = Convert.ToInt32(TextBoxIlosc5.Text);
                suma5 = cena5 * ilosc5;
            }
            if (Request.Cookies["ciasteczko6"] != null)
            {
                cena6 = Convert.ToSingle(LabelIlosc6.Text);
                ilosc6 = Convert.ToInt32(TextBoxIlosc6.Text);
                suma6 = cena6 * ilosc6;
            }
            if (Request.Cookies["ciasteczko7"] != null)
            {
                cena7 = Convert.ToSingle(LabelIlosc7.Text);
                ilosc7 = Convert.ToInt32(TextBoxIlosc7.Text);
                suma7 = cena7 * ilosc7;
            }
            if (Request.Cookies["ciasteczko8"] != null)
            {
                cena8 = Convert.ToSingle(LabelIlosc8.Text);
                ilosc8 = Convert.ToInt32(TextBoxIlosc8.Text);
                suma8 = cena8 * ilosc8;
            }
            if (Request.Cookies["ciasteczko9"] != null)
            {
                cena9 = Convert.ToSingle(LabelIlosc9.Text);
                ilosc9 = Convert.ToInt32(TextBoxIlosc9.Text);
                suma9 = cena9 * ilosc9;
            }
            if (Request.Cookies["ciasteczko10"] != null)
            {
                cena10 = Convert.ToSingle(LabelIlosc10.Text);
                ilosc10 = Convert.ToInt32(TextBoxIlosc10.Text);
                suma10 = cena10 * ilosc10;
            }
            suma = suma1 + suma2 + suma3 + suma4 + suma5 + suma6 + suma7 + suma8 + suma9 + suma10;
            labelSuma.InnerText = Convert.ToString(suma);
        }

        protected void TextBoxIlosc1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ButtonAktualizuj1_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc1"].Value = TextBoxIlosc1.Text;//przyporządkowanie dla ciasteczka zmiennej
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj2_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc2"].Value = TextBoxIlosc2.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj3_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc3"].Value = TextBoxIlosc3.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj4_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc4"].Value = TextBoxIlosc4.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj5_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc5"].Value = TextBoxIlosc5.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj6_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc6"].Value = TextBoxIlosc6.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj8_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc8"].Value = TextBoxIlosc8.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj7_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc7"].Value = TextBoxIlosc7.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj9_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc9"].Value = TextBoxIlosc9.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

        protected void ButtonAktualizuj10_Click(object sender, EventArgs e)
        {
            Response.Cookies["ilosc10"].Value = TextBoxIlosc10.Text;
            Page.Response.Redirect("/Account/Koszyk1.aspx");
        }

       
    }//koniec
    
}