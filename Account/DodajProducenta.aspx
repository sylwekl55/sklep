﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DodajProducenta.aspx.cs" Inherits="sklepmaster.Account.DodajProducenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="nav-justified">
		<tr>
			<td>
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNazwaFirmy">Nazwa Firmy</span>
					<asp:TextBox ID="TextBoxNazwaFirmy"  runat="server"  placeholder="Nazwa Firmy" aria-describedby="labelNazwaFirmy" style="width: 250px;" ></asp:TextBox>
				    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxNazwaFirmy" ErrorMessage="Podaj Nazwę firmy" ForeColor="Red">*</asp:RequiredFieldValidator>
				    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxNazwaFirmy" ErrorMessage="Podaj prawidłową nazwę firmy" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNIP">NIP</span>
					<asp:TextBox ID="TextBoxNIP"  runat="server"  placeholder="NIP" aria-describedby="NIP" style="width: 250px;" ></asp:TextBox>
				    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNIP" ErrorMessage="podaj nip" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxNIP" ErrorMessage="Podajnumer NIP xxxxxxxxxx" ValidationExpression="^[0-9]{10,10}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelREGON">REGON</span>
					<asp:TextBox ID="TextBoxREGON"  runat="server"  placeholder="REGON" aria-describedby="labelREGON" style="width: 250px;" ></asp:TextBox>
				    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxREGON" ErrorMessage="Podaj numer regon" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxREGON" ErrorMessage="Podaj numer REGON xxxxxxxxx" ValidationExpression="^\d{9}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>

		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelEmail">E-mail</span>
					<asp:TextBox ID="TextBoxEmail" runat="server" class="form-control" placeholder="Email" aria-describedby="labelEmail" Style="width: 250px; top: 0px; left: 0px;"></asp:TextBox>
				    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Podaj adres e-mail" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Podaj adres e-mail" ValidationExpression="^[a-zA-Z1-9]{1,}@[a-zA-Z1-9]{1,}.[a-zA-Z1-9]{2,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelUlica">Ulica</span>
					<asp:TextBox ID="TextBoxUlica" runat="server" class="form-control" placeholder="Ulica" aria-describedby="labelUlica" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxUlica" ErrorMessage="Podaj nazwę ulicy" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBoxUlica" ErrorMessage="Podaj nazwę ulicy" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>

			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrLokalu">Nr.Lokalu</span>
					<asp:TextBox ID="TextBoxNrLokalu" runat="server" class="form-control" placeholder="Nr.Lokalu" aria-describedby="labelNrLokalu" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxNrLokalu" ErrorMessage="Podaj nazwę lokalu" ForeColor="Red" ViewStateMode="Inherit">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBoxNrLokalu" ErrorMessage="Podaj numer lokalu" ValidationExpression="^[1-9]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrMieszkania">Nr.Mieszkania</span>
					<asp:TextBox ID="TextBoxNrMieszkania" runat="server" class="form-control" placeholder="Nr.Mieszkania" aria-describedby="labelNrMieszkania" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBoxNrMieszkania" ErrorMessage="Podaj numer mieszkania" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBoxNrMieszkania" ErrorMessage="Podaj numer mieszkania" ValidationExpression="^[1-9]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKodPocztowy">Kod Pocztowy</span>
					<asp:TextBox ID="TextBoxKodPocztowy" runat="server" class="form-control" placeholder="Kod Pocztowy" aria-describedby="labelKodPocztowy" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBoxKodPocztowy" ErrorMessage="Podaj kod ocztowy" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBoxKodPocztowy" ErrorMessage="Podaj kod pocztowy xx-xxx" ValidationExpression="^[0-9]{2}-[0-9]{3}"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelMiejscowosc">Miejscowość</span>
					<asp:TextBox ID="TextBoxMiejscowosc" runat="server" class="form-control" placeholder="Miejscowosc" aria-describedby="labelMiejscowosc" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxMiejscowosc" ErrorMessage="Podaj miejscowość" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBoxMiejscowosc" ErrorMessage="Podaj nazwę miejscowości" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelPowiat">Powiat</span>
					<asp:TextBox ID="TextBoxPowiat" runat="server" class="form-control" placeholder="Powiat" aria-describedby="labelPowiat" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxPowiat" ErrorMessage="Podaj Powiat" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="TextBoxPowiat" ErrorMessage="Podaj nazwę Powiatu" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelWojewudztwo">Województwo</span>
					<asp:TextBox ID="TextBoxWojewudztwo" runat="server" class="form-control" placeholder="Wojewudztwo" aria-describedby="labelWojewudztwo" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="TextBoxWojewudztwo" ErrorMessage="Podaj nazwę województwa" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="TextBoxWojewudztwo" ErrorMessage="Podaj nazwę Wojewudztwa" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKraj">Kraj</span>
					<asp:TextBox ID="TextBoxKraj" runat="server" class="form-control" placeholder="Kraj" aria-describedby="labelKraj" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="TextBoxKraj" ErrorMessage="Podaj nazwę kraju" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="TextBoxKraj" ErrorMessage="Podaj nazwę Kraju" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrTelefonu">Nr.Telefonu</span>
					<asp:TextBox ID="TextBoxNrTelefonu" runat="server" class="form-control" placeholder="Nr.Telefonu" aria-describedby="labelNrTelefonu" Style="width: 250px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxNrTelefonu" ErrorMessage="Podaj numer telefonu" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="TextBoxNrTelefonu" ErrorMessage="Podaj numer telefonu" ValidationExpression="^[0-9]{9}"></asp:RegularExpressionValidator>
				</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="labelBlad" runat="server" Text=""></asp:Label>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			
			<td>
				<asp:Button class="btn btn-default btn-primary" ID="ButtonDodaj" runat="server" Text="Dodaj" OnClick="ButtonDodaj_Click" />
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</asp:Content>
