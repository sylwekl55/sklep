﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StartowaSzczegulyZdjecia.aspx.cs" Inherits="sklepmaster.Account.StartowaSzczegulyZdjecia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head  runat="server">
  
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: inherit;">
								<tr>
									<th></th>
								</tr>
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>	
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
							<td>
                                 
                                  <asp:ImageButton runat="server" PostBackUrl='<%# String.Format("StartowaSzczeguly.aspx?{0}", Eval("produkt")) %>' ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) %> ' />
							</td>
							
						</ItemTemplate>

				</asp:ListView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
