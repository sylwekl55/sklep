﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Logowanie.aspx.cs" Inherits="sklepmaster.Account.Logowanie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
        .auto-style3 {
            width: 6px;
        }
    .auto-style4 {
		width: 6px;
		height: 36px;
	}
	.auto-style5 {
		height: 36px;
	}
    	.auto-style6 {
			width: 6px;
			height: 21px;
		}
		.auto-style7 {
			height: 21px;
		}
        .auto-style8 {
            width: 6px;
            height: 22px;
        }
        .auto-style9 {
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" colspan="3"><span id="result_box" class="short_text" lang="pl"><span class="hps">Użyj</span> <span class="">loginu i hasła, aby</span> <span class="hps">zalogować</span><span>.</span></span></td>
        </tr>
        <tr>
            <td class="auto-style8"></td>
            <td class="auto-style9"></td>
            <td class="auto-style9"></td>
        </tr>
        <tr>
			<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelLogin">Login</span>
						<asp:TextBox ID="TextBoxLogin" runat="server" class="form-control" placeholder="Login" aria-describedby="labelLogin" style="width: 250px; top: 0px; left: 0px;" ></asp:TextBox>
					</div>
			</td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxLogin" ErrorMessage="* Podaj Login " ForeColor="Red" ValidationGroup="logowanie"></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
			<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelHaslo">Haslo</span>
						<asp:TextBox ID="TextBoxHaslo"  TextMode="Password" runat="server" class="form-control" placeholder="Haslo" aria-describedby="labelHaslo" style="width: 250px;"></asp:TextBox>
					</div>
			</td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Podaj Hasło" ControlToValidate="TextBoxHaslo" ForeColor="Red" ValidationGroup="logowanie"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">
            </td>
            <td class="auto-style5">
                <asp:Button class="btn btn-default btn-primary" ID="buttonZaloguj" ValidationGroup="logowanie" runat="server" Text="Zaloguj" OnClick="buttonZaloguj_Click" Width="123px" />
            </td>
            <td class="auto-style5">
                <% if (!sklepmaster.baza.DaneZalogowanejOsoby.CzyZalogowany(Session["klient"]) ) { %>
							<asp:Button class="btn btn-default btn-primary" ID="buttonRejestracja" runat="server" Text="Rejestracja" OnClick="buttonRejestracja_Click" />
				<%} %>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6" colspan="2">
                <asp:Label ID="labelBlad" runat="server" Text=""></asp:Label>
            </td>
            <td class="auto-style7"></td>
            <td class="auto-style7"></td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
