﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rejestracja.aspx.cs" Inherits="sklepmaster.Account.Rejestracja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style10 {
			width: 491px;
		}
		.auto-style11 {
			width: 490px;
		}
        .auto-style12 {
            width: 490px;
            height: 22px;
        }
        .auto-style13 {
            height: 22px;
        }
        .tabele {
            border-radius: 16px !important;
            border="0" ;
            border-spacing: 0mm 2mm;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>&nbsp;</h2>
   
    <h4>Stwórz nowe konto</h4>
    <hr />
	
		<table class=" tabele auto-style1" >
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelImie">Imię</span>
						<asp:TextBox style="width: 250px;" ID="TextBoxImie" runat="server" class="form-control" placeholder="Imię" aria-describedby="labelImie"></asp:TextBox>
                        					</div>
				</td>
				<td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxImie" ErrorMessage="Podaj imię" ForeColor="Red">*</asp:RequiredFieldValidator>
				        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxImie" ErrorMessage="Podaj imię" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

					&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelNazwisko">Nazwisko</span>
						<asp:TextBox style="width: 250px;" ID="TextBoxNazwisko" runat="server" class="form-control" placeholder="Nazwisko" aria-describedby="labelNazwisko"></asp:TextBox>
 					</div>
				
					
				</td>
				<td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxNazwisko" ErrorMessage="Podaj nazwisko" ForeColor="Red">*</asp:RequiredFieldValidator>
				        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxNazwisko" ErrorMessage="Podaj Nazwisko" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

				</td>
			</tr>
			<tr >
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelLogin">Login</span>
						<asp:TextBox ID="TextBoxLogin" runat="server" class="form-control" placeholder="Login" aria-describedby="labelLogin" style="width: 250px;"></asp:TextBox>
					</div>
				</td>
				<td>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxLogin" ErrorMessage="Podaj login" ForeColor="Red">*</asp:RequiredFieldValidator>
				        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxLogin" ErrorMessage="Podaj login" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelHaslo">Hasło</span>
						<asp:TextBox ID="TextBoxHaslo" TextMode="Password" runat="server" class="form-control" placeholder="Hasło" aria-describedby="labelHaslo" style="width: 250px; top: 1px; left: 0px;"></asp:TextBox>
                        
					</div>
				</td>
				<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxHaslo" ErrorMessage="Podaj Hasło" ForeColor="Red">*</asp:RequiredFieldValidator>
				    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxHaslo" ErrorMessage="Podaj Hasło min 6 liter znaki duże małe litery oraz cyfry" ValidationExpression="^[a-zA-Z1-9 -]{4,}$" ForeColor="Red"></asp:RegularExpressionValidator>
				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<asp:CompareValidator class="label label-danger" ID="CompareValidator1" runat="server" ControlToCompare="TextBoxHaslo" ControlToValidate="TextBoxPowtorzHaslo" ErrorMessage="hasła nie są jednakowe"></asp:CompareValidator>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelPowtorzHaslo">Powtórz Hasło</span>
						<asp:TextBox ID="TextBoxPowtorzHaslo"  runat="server" class="form-control" placeholder="Powtórz Hasło" aria-describedby="labelHaslo" style="width: 250px; top: 0px; left: -1px;" TextMode="Password"></asp:TextBox>
					</div>
				</td>
				<td>
					    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="TextBoxHaslo" ControlToValidate="TextBoxPowtorzHaslo" ErrorMessage="hasła są różne" ForeColor="Red"></asp:CompareValidator>
				</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelNazwaFirmy">Nazwa firmy</span>
						<asp:TextBox ID="TextBoxNazwaFirmy"  runat="server" class="form-control" placeholder="Nazwa firmy" aria-describedby="labelNazwaFirmy" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td>
                    				</td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelRegon">Regon</span>
						<asp:TextBox ID="TextBoxRegon"  runat="server" class="form-control" placeholder="Regon" aria-describedby="labelRegon" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td></td>
			</tr>
			<tr>
				<td class="auto-style11">
					<div class="input-group">
						<span style="width: 200px;" class="input-group-addon" id="labelNIP">NIP</span>
						<asp:TextBox ID="TextBoxNIP"  runat="server" class="form-control" placeholder="NIP" aria-describedby="labelNIP" style="width: 250px;" ></asp:TextBox>
					</div>
				</td>
				<td></td>
			</tr>
			<tr>
				<td class="auto-style11">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		
	<table class=" tabele auto-style1" border="0">
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelEmail">Email</span>
					<asp:TextBox ID="TextBoxEmail" runat="server" class="form-control" placeholder="Email" aria-describedby="labelEmail" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Podaj adres e-mail" ForeColor="Red">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Podaj adres e-mail" ValidationExpression="^[a-zA-Z0-9]{1,}@[a-zA-Z0-9]{0,}.[a-zA-Z0-9]{2,}$" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelUlica">Ulica</span>
					<asp:TextBox ID="TextBoxUlica" runat="server" class="form-control" placeholder="Ulica" aria-describedby="labelUlica" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxUlica" ErrorMessage="Podaj nazwę ulicy" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBoxUlica" ErrorMessage="Podaj nazwę ulicy" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

			</td>
		</tr>
		<tr>

			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrLokalu">Nr.Lokalu</span>
					<asp:TextBox ID="TextBoxNrLokalu" runat="server" class="form-control" placeholder="Nr.Lokalu" aria-describedby="labelNrLokalu" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxNrLokalu" ErrorMessage="Podaj nazwę lokalu" ForeColor="Red" ViewStateMode="Inherit">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="TextBoxNrLokalu" ErrorMessage="Podaj numer lokalu" ValidationExpression="^[1-9]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrMieszkania">Nr.Mieszkania</span>
					<asp:TextBox ID="TextBoxNrMieszkania" runat="server" class="form-control" placeholder="Nr.Mieszkania" aria-describedby="labelNrMieszkania" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="TextBoxNrMieszkania" ErrorMessage="Podaj numer mieszkania" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="TextBoxNrMieszkania" ErrorMessage="Podaj numer mieszkania" ValidationExpression="^[1-9]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKodPocztowy">Kod Pocztowy</span>
					<asp:TextBox ID="TextBoxKodPocztowy" runat="server" class="form-control" placeholder="Kod Pocztowy" aria-describedby="labelKodPocztowy" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="TextBoxKodPocztowy" ErrorMessage="Podaj kod ocztowy" ForeColor="Red">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="TextBoxKodPocztowy" ErrorMessage="Podaj kod pocztowy xx-xxx" ValidationExpression="^[0-9]{2}-[0-9]{3}" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelMiejscowosc">Miejscowość</span>
					<asp:TextBox ID="TextBoxMiejscowosc" runat="server" class="form-control" placeholder="Miejscowość" aria-describedby="labelMiejscowosc" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxMiejscowosc" ErrorMessage="Podaj miejscowość" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="TextBoxMiejscowosc" ErrorMessage="Podaj nazwę miejscowości" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelPowiat">Powiat</span>
					<asp:TextBox ID="TextBoxPowiat" runat="server" class="form-control" placeholder="Powiat" aria-describedby="labelPowiat" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxPowiat" ErrorMessage="Podaj Powiat" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="TextBoxPowiat" ErrorMessage="Podaj nazwę Powiatu" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelWojewudztwo">Województwo</span>
					<asp:TextBox ID="TextBoxWojewudztwo" runat="server" class="form-control" placeholder="Województwo" aria-describedby="labelWojewudztwo" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBoxWojewudztwo" ErrorMessage="Podaj nazwę województwa" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ControlToValidate="TextBoxWojewudztwo" ErrorMessage="Podaj nazwę Wojewudztwa" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style12">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelKraj">Kraj</span>
					<asp:TextBox ID="TextBoxKraj" runat="server" class="form-control" placeholder="Kraj" aria-describedby="labelKraj" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td class="auto-style13">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="TextBoxKraj" ErrorMessage="Podaj nazwę kraju" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" ControlToValidate="TextBoxKraj" ErrorMessage="Podaj nazwę Kraju" ValidationExpression="^[a-zA-Z1-9 -ąśćóńłŃŁÓĄŚĆ]{1,}$" ForeColor="Red"></asp:RegularExpressionValidator>

			</td>
		</tr>
		<tr>
			<td class="auto-style11">
				<div class="input-group">
					<span style="width: 200px;" class="input-group-addon" id="labelNrTelefonu">Nr.Telefonu</span>
					<asp:TextBox ID="TextBoxNrTelefonu" runat="server" class="form-control" placeholder="Nr.Telefonu" aria-describedby="labelNrTelefonu" Style="width: 250px;"></asp:TextBox>
				</div>
			</td>
			<td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="TextBoxNrTelefonu" ErrorMessage="Podaj numer telefonu" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" ControlToValidate="TextBoxNrTelefonu" ErrorMessage="Podaj numer telefonu" ValidationExpression="^[0-9]{9}" ForeColor="Red"></asp:RegularExpressionValidator>
			</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style10">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
    <p>
        <asp:Label ID="labelBlad" runat="server" Text=""></asp:Label>
    </p>
    <p>
        <asp:Button class="btn btn-default btn-primary" ID="ButtonZarejestruj" runat="server" Text="Zarejestruj" OnClick="ButtonZarejestruj_Click" />
    </p>
    

</asp:Content>
