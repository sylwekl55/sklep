﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Koszyk1.aspx.cs" Inherits="sklepmaster.Account.Koszyk1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .auto-style1 {
            width: 34px;
        }
        .auto-style2 {
            width: 35px;
        }
        .auto-style3 {
            width: 110px;
        }
        .table-produkty1{
            border-radius: 16px;
            background-color: #caf5ff;
        }
        .table-produkty {
            border-radius: 16px;
        }
        .table-produkty td{
            vertical-align: middle !important;/*important aby inne style nie nadpisywały*/
        }
        .table-produkty1 td{
            padding: 0px !important;
            background-color: #8adff2;
        }
        .table-produkty1 input{
            width: 100%;
            height: 84px;
            border:none;
            background-color: #8adff2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="LabelBland" runat="server" ForeColor="Red"></asp:Label>
    <table border="0" style="height: inherit;" class="table table-produkty" >
        <tr>
            <td class="auto-style4">
                &nbsp;Nazwa
            </td>
            <td class="auto-style5">
                &nbsp;Cena
            </td>
            <td class="auto-style6" >
                <div align="right" style="width: 305px">ilość / Aktualna Ilość Dostępna      </td></div>
                 
            <td class="auto-style4">
                
            </td>
        </tr>


        <tr>
            <td>
                <asp:Label ID="LabelNazwa1" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc1" runat="server" EnableTheming="True">1</asp:TextBox>
                <asp:Label ID="LabelIlosc1" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_1" runat="server" Text="/"></asp:Label>
                <asp:Label ID="LabelIloscMax1" runat="server" Text="Label"></asp:Label>
               
            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj1" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj1_Click1" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsuń1" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko1" OnClick="ButtonUsuń1_Click" />
            </td>
        </tr>


        <tr>
            <td>
                <asp:Label ID="LabelNazwa2" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena2" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc2" runat="server">1</asp:TextBox>
                <asp:Label ID="LabelIlosc2" runat="server" Text="ilosc"></asp:Label> 
                 <asp:Label ID="Label_2" runat="server" Text="/">
                 </asp:Label> <asp:Label ID="LabelIloscMax2" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj2" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj2_Click1" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun2" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko2" OnClick="ButtonUsun2_Click" />
                </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="LabelNazwa3" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena3" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc3" runat="server">1</asp:TextBox> 
                <asp:Label ID="LabelIlosc3" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_3" runat="server" Text="/"></asp:Label> 
                <asp:Label ID="LabelIloscMax3" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj3" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj3_Click1" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun3" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko3" OnClick="ButtonUsun3_Click" />
            </td>
        </tr>


        		<tr>
            <td>
                <asp:Label ID="LabelNazwa4" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena4" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc4" runat="server">1</asp:TextBox> 
                <asp:Label ID="LabelIlosc4" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_4" runat="server" Text="/"></asp:Label>
                <asp:Label ID="LabelIloscMax4" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj4" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj4_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun4" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko4" OnClick="ButtonUsun4_Click" />
            </td>
        </tr>
		
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa5" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena5" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc5" runat="server">1</asp:TextBox> 
                <asp:Label ID="LabelIlosc5" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_5" runat="server" Text="/">
                </asp:Label><asp:Label ID="LabelIloscMax5" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj5" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj5_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun5" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko5" OnClick="ButtonUsun5_Click" />
            </td>
        </tr>
		
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa6" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena6" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">
                <asp:TextBox ID="TextBoxIlosc6" runat="server">1</asp:TextBox>
                <asp:Label ID="LabelIlosc6" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_6" runat="server" Text="/"></asp:Label>
                <asp:Label ID="LabelIloscMax6" runat="server" Text="Label"></asp:Label>
            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj6" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj6_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun6" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko6" OnClick="ButtonUsun6_Click" />
            </td>
        </tr>
		
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa7" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena7" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc7" runat="server">1</asp:TextBox>
                <asp:Label ID="LabelIlosc7" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_7" runat="server" Text="/"></asp:Label> 
                <asp:Label ID="LabelIloscMax7" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj7" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj7_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun7" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko7" OnClick="ButtonUsun7_Click" />
            </td>
        </tr>
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa8" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena8" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc8" runat="server">1</asp:TextBox> 
                <asp:Label ID="LabelIlosc8" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_8" runat="server" Text="/"></asp:Label>
                <asp:Label ID="LabelIloscMax8" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj8" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj8_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun8" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko8" OnClick="ButtonUsun8_Click" />
            </td>
        </tr>
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa9" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena9" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc9" runat="server">1</asp:TextBox> 
                <asp:Label ID="LabelIlosc9" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_9" runat="server" Text="/"></asp:Label>
                <asp:Label ID="LabelIloscMax9" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj9" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj9_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun9" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko9" OnClick="ButtonUsun9_Click" />
            </td>
        </tr>
		
		
		<tr>
            <td>
                <asp:Label ID="LabelNazwa10" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style3">
                &nbsp;<asp:Label ID="LabelCena10" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="auto-style7">

                <asp:TextBox ID="TextBoxIlosc10" runat="server">1</asp:TextBox>
                <asp:Label ID="LabelIlosc10" runat="server" Text="ilosc"></asp:Label> 
                <asp:Label ID="Label_10" runat="server" Text="/"></asp:Label> 
                <asp:Label ID="LabelIloscMax10" runat="server" Text="Label"></asp:Label>

            </td>
            <td>
                <asp:Button ID="ButtonAktualizuj10" runat="server" Text="Aktualizuj" OnClick="ButtonAktualizuj10_Click" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonUsun10" runat="server" Text="Usuń" OnClientClick="UsunCiasteczko10" OnClick="ButtonUsun10_Click" />
            </td>
        </tr>
		


        
        <tr>
            <td>
                <asp:Button ID="ButtonUsuń" class="btn btn-default btn-primary" runat="server" Text="Wyczyść Koszyk " OnClientClick="ButtonWyczysc" OnClick="ButtonUsuń_Click"/>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style7">
                <div class="input-group">
                    <asp:Button class="btn btn-default btn-primary" ID="ButtonSuma" runat="server" Text="Zsumuj" OnClick="ButtonSuma_Click" />
				    <span  class="input-group-addon" id="labelSuma" runat="server"></span>
				</div>
            </td>
            <td></td>
            <td>
                <div class="input-group">
                    <asp:Button class="btn btn-default btn-primary" ID="ButtonKup" runat="server" Text="Kup " OnClientClick="ButtonKup" OnClick="ButtonKup_Click"/>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>