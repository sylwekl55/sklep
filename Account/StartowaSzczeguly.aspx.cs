﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
	public partial class StartowaSzczeguly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//Label1.Text=this.ClientQueryString;
			if (!Page.IsPostBack)
			{
				this.BindListView();
				this.BindListView1();
				//this.BindListView2();

			}
		}
		private void BindListView2()
		{
			DataTable dataTable = new DataTable();

			

			string sqlCommand = "SELECT id_fotografii, zdjecie FROM fotografia WHERE id_fotografii=" + Convert.ToInt32(this.ClientQueryString) + ";";
			try
			{
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
					{
						using (var adapter = new MySqlDataAdapter(command))
						{
							adapter.Fill(dataTable);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error reading data table [" + ex.Message + "]", ex);
			}
			foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
			{
				// ... Write value of first field as integer.
				//Console.WriteLine(row.Field<int>(0));
			}



			ListView1.DataSource = dataTable;
			//foreach (var asd in lvCustomers.DataSource)
			//{
			////	asd.
			//}
			ListView1.DataBind();

		}
		private void BindListView1()
		{
			DataTable dataTable = new DataTable();

            string sqlCommand = "SELECT id_produktu, nazwa_produktu, ilosc_w_magazynie, wersja, opis, cena_brutto, producent, vat, nazwa_promocji, wysokosc_promocji, nazwa_firmy, nip, regon, kraj, kod_pocztowy, wojewodztwo, nr_mieszkania, email, miejscowosc, numer FROM promocje, sklep.produkt p, producent pp, adres a WHERE p.producent=pp.id_producent AND a.id_adresu=pp.adres and promocje.id_promocji=promocje AND id_produktu=" + Convert.ToInt32(this.ClientQueryString) + ";";

			try
			{
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
					{
						using (var adapter = new MySqlDataAdapter(command))
						{
							adapter.Fill(dataTable);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error reading data table [" + ex.Message + "]", ex);
			}
			foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
			{
				// ... Write value of first field as integer.
				//Console.WriteLine(row.Field<int>(0));
			}



			ListView1.DataSource = dataTable;
			//foreach (var asd in lvCustomers.DataSource)
			//{
			////	asd.
			//}
			ListView1.DataBind();
		
		}

		private void BindListView()
		{
			DataTable dataTable = new DataTable();

			string sqlCommand = "SELECT id_fotografii, zdjecie FROM fotografia WHERE produkt=" + Convert.ToInt32(this.ClientQueryString) + " AND zdjecie is not null;";

			try
			{
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
                    polaczenie.Open();

                    using (var command = new MySqlCommand(sqlCommand, polaczenie))
					{
						using (var adapter = new MySqlDataAdapter(command))
						{
							adapter.Fill(dataTable);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error reading data table [" + ex.Message + "]", ex);
			}
			foreach (DataRow row in dataTable.Rows) // przejście po wszystkich elementach tablicy 2 wymiarowej 
			{
				// ... Write value of first field as integer.
				//Console.WriteLine(row.Field<int>(0));
			}



			lvCustomers.DataSource = dataTable;
			//foreach (var asd in lvCustomers.DataSource)
			//{
			////	asd.
			//}
			lvCustomers.DataBind();
		}

		
	}
}