﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
	public partial class EdycjaDanych : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            Edycja();
		}

        private void Edycja()
        {
            sklepmaster.baza.Logowanie logowanie = new sklepmaster.baza.Logowanie();
            sklepmaster.baza.DaneZalogowanejOsoby dane = (sklepmaster.baza.DaneZalogowanejOsoby)Session["klient"];

            //TextBoxUlica.Attributes.Add(TextBoxUlica.Text , (string)Session["Ulica"] );

            TextBoxNazwaFirmy.Text = (string)Session["NazwaFirmy"];
            TextBoxRegon.Text = (string)Session["Regon"];
            TextBoxUlica.Text = (string)Session["Ulica"];
            TextBoxNIP.Text = (string)Session["Nip"];
            TextBoxNrLokalu.Text = (string)Session["NrLokalu"].ToString();
            TextBoxNrMieszkania.Text = (string)Session["NrMieszkania"].ToString();
            TextBoxKodPocztowy.Text = (string)Session["KodPocztowy"];
            TextBoxMiejscowosc.Text = (string)Session["Miejscowowsc"];
            TextBoxPowiat.Text = (string)Session["Powiat"];
            TextBoxWojewudztwo.Text = (string)Session["Wojewodztwo"];
            TextBoxKraj.Text = (string)Session["Kraj"];
            TextBoxNrTelefonu.Text = (string)Session["NrTelefonu"].ToString();

           
            //labelBlad.Text = Session["Login"].ToString();
            
            this.LabelNazwaFirmy1.Text = (string)Session["NazwaFirmy"];
            this.labelRegon1.Text = (string)Session["Regon"];
            this.labelUlica1.Text = (string)Session["Ulica"];
            this.labelNIP1.Text = (string)Session["Nip"];
            this.labelNrLokalu1.Text = (string)Session["NrLokalu"].ToString();
            this.labelNrMieszkania1.Text = (string)Session["NrMieszkania"].ToString();
            this.labelKodPocztowy1.Text = (string)Session["KodPocztowy"];
            this.labelMiejscowosc1.Text = (string)Session["Miejscowowsc"];
            this.labelPowiat1.Text = (string)Session["Powiat"];
            this.labelWojewudztwo1.Text = (string)Session["Wojewodztwo"];
            this.labelKraj1.Text = (string)Session["Kraj"];
            this.labelNrTelefonu1.Text = (string)Session["NrTelefonu"].ToString();
            this.labelBlad.Text = (string)Session["IdOsoba"].ToString() + " oraz id adres: " + (string)Session["IdAdres"].ToString();

        }

        protected void ButtonZmien_Click(object sender, EventArgs e)
        {
            int nr_lokalu, nr_mieszkania, numer;

            if (!(Int32.TryParse(TextBoxNrLokalu.Text, out nr_lokalu))) //konwertuje na int z stringa 
            {
                nr_lokalu = 0;
            }

            if (!(Int32.TryParse(TextBoxNrMieszkania.Text, out nr_mieszkania)))
            {
                nr_mieszkania = 0;
            }

            if (!(Int32.TryParse(TextBoxNrTelefonu.Text, out numer)))
            {
                numer = 0;
            }

            RejestracjaKlienta r = new RejestracjaKlienta();

            if (TextBoxNazwaFirmy.Text.Length != 0)
            {
                r.NazwaFirmy = TextBoxNazwaFirmy.Text;
            }
            else
            {
                r.NazwaFirmy = (string)Session["NazwaFirmy"];
            }
            
            if (TextBoxNIP.Text.Length != 0)
            {
                r.Nip = TextBoxNIP.Text;
            }
            else
            {
                r.Nip = (string)Session["Nip"].ToString();
            }

            if (TextBoxUlica.Text.Length != 0)
            {
                r.Ulica = TextBoxUlica.Text;
            }
            else
            {
                r.Ulica = (string)Session["Ulica"];
            }

            if (nr_lokalu != 0)
            {
                r.NrLokalu = nr_lokalu;
            }
            else
            {
                r.NrLokalu = (int)Session["NrLokalu"];
            }
            
            if (nr_mieszkania != 0)
            {
                r.NrMieszkania = nr_mieszkania;
            }
            else
            {
                r.NrMieszkania = (int)Session["NrMieszkania"];
            }

            if (TextBoxKodPocztowy.Text.Length != 0)
            {
                r.KodPocztowy = TextBoxKodPocztowy.Text;
            }
            else
            {
                r.KodPocztowy = (string)Session["KodPocztowy"];
            }

            if (TextBoxMiejscowosc.Text.Length != 0)
            {
                r.Miejscowosc = TextBoxMiejscowosc.Text;
            }
            else
            {
                r.Miejscowosc = (string)Session["Miejscowowsc"];
            }
            
            if (TextBoxPowiat.Text.Length != 0)
            {
                r.Powiat = TextBoxPowiat.Text;
            }
            else { r.Powiat = (string)Session["Powiat"]; }

            if (TextBoxWojewudztwo.Text.Length != 0)
            {
                r.Wojewodztwo = TextBoxWojewudztwo.Text;
            }
            else { r.Wojewodztwo = (string)Session["Wojewodztwo"]; }

            if (TextBoxWojewudztwo.Text.Length != 0)
            {
                r.Wojewodztwo = TextBoxWojewudztwo.Text;
            }
            else { r.Wojewodztwo = (string)Session["Wojewodztwo"]; }

            if (TextBoxKraj.Text.Length != 0)
            {
                r.Kraj = TextBoxKraj.Text;
            }
            else { r.Kraj = (string)Session["Kraj"]; }

            if (numer != 0)
            {
                r.Numer = numer;
            }
            else { r.Numer = (int)Session["NrTelefonu"]; };

            if (TextBoxRegon.Text.Length != 0)
            {
                r.Regon = TextBoxRegon.Text;
            }
            else { r.Regon = (string)Session["Regon"]; ; }

            if (TextBoxHaslo.Text.Length != 0)
            {
                r.Haslo = TextBoxHaslo.Text;
                r.CzyZmianaHasla = true;
            }
            else { r.CzyZmianaHasla = false; }

            r.IdAdres = (int)Session["IdAdres"];
            r.IdOsoba = (int)Session["IdOsoba"];
            r.Login = (string)Session["Login"];
            
            
          

            try
            {
                if (!r.Edytuj())
                {
                    labelBlad.Text = "Błąd dodawania klienta: " + r.PowodNieZarejestrowania;
                    return;
                }

                labelBlad.Text = "Zmieniono Dane Osobowe ";
            }
            catch (Exception ex)
            {
                labelBlad.Text = "Błąd dodawania klienta: " + ex.GetType().Name + " [" + ex.Message + "]";
                return;
            }

            Session["NazwaFirmy"] = r.NazwaFirmy;
            Session["Regon"] = r.Regon;
            Session["Ulica"] = r.Ulica;
            Session["Nip"] = r.Nip;
            Session["NrLokalu"] = r.NrLokalu;
            Session["NrMieszkania"] = r.NrMieszkania;
            Session["KodPocztowy"] = r.KodPocztowy;
            Session["Miejscowowsc"] = r.Miejscowosc;
            Session["Powiat"] = r.Powiat;
            Session["Wojewodztwo"] = r.Wojewodztwo;
            Session["Kraj"] = r.Kraj;
            Session["NrTelefonu"] = r.Numer;

            Page.Response.Redirect("/Account/EdycjaDanych.aspx");

        }

      
		
		
	}
}