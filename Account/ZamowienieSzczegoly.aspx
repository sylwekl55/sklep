﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ZamowienieSzczegoly.aspx.cs" Inherits="sklepmaster.Account.ZamowienieSzczegoly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
		
		 body 
     {
        margin:0;
        padding:0;
        height:100%; 
        overflow-y:auto;  
     }
     .modal
     {
        display: none; 
        position: absolute;
        top: 0px; 
        left: 0px;
        background-color:black;
        z-index:100;
        opacity: 0.8;
        filter: alpha(opacity=60);
        -moz-opacity:0.8;
        min-height: 100%;
     }
     #divImage
     {
        display: none;
        z-index: 1000;
        position: fixed;
        top: 0;
        left: 0;
        background-color:White;
        height: 550px;
        width: 600px;
        padding: 3px;
        border: solid 1px black;
     }
     * html #divImage {position:absolute;}
		.auto-style3 {
			width: 701px
		}
    .bezBorder{
        border: none !important;
    }
    .bezBorder tr{
        border: none !important;
    }
    .bezBorder th{
        border: none !important;
    }
    .bezBorder td{
        border: none !important;
    }
	    .auto-style4 {
            width: 747px
        }
	</style>


	



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	
	
		<div id="divBackground" class="modal">
		</div>
		<div id="divImage" class="info">
			<table style="height: 100%; width: 100%" class="table bezBorder">
				<tr>
					
				</tr>
				<tr>
					<td >
						
					</td>
				</tr>
			</table>


		</div>

	<table class="table bezBorder">
		<tr>
		
			<td class="auto-style4">
				<asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: inherit;" class="table bezBorder">
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
							<tr>
							    <td>
                                    Imię: &nbsp&nbsp&nbsp 
									<%# Eval("imie") %>
								</td>
                            </tr>
                            </tr>
								<td>
                                    Nazwisko: &nbsp&nbsp&nbsp 
									<%# Eval("nazwisko") %>
								</td>
                            </tr>
                            </tr>
								<td>
                                    Miejscowość: &nbsp&nbsp&nbsp 
									<%# Eval("miejscowosc") %>
								</td>
                            </tr>
                            </tr>
                                <td>
                                    Ulica: &nbsp&nbsp&nbsp 
									<%# Eval("ulica") %>
								</td>
                            </tr>
                            </tr>
                                <td>
                                    Nr. Domu: &nbsp&nbsp&nbsp 
									<%# Eval("nr_mieszkania") %>
								</td>
                            </tr>
                            </tr>
                                <td>
                                    Nr. Mieszkania: &nbsp&nbsp&nbsp 
									<%# Eval("nr_lokalu") %>
								</td>
                            </tr>
                            </tr>
                                <td>
                                    Województwo: &nbsp&nbsp&nbsp 
									<%# Eval("wojewodztwo") %>
								</td>
                            </tr>
                            </tr>
                                <td>
                                    Email: &nbsp&nbsp&nbsp 
									<%# Eval("email") %>
								</td>
							<%--<td>
									<%# Eval("opis") %>
								</td>--%>
						</ItemTemplate>

				</asp:ListView>

			</td>
			<td class="auto-style3">
				<asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: 100%;">
								
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									<td colspan="3">
										<asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
											<Fields>
												<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
													ShowNextPageButton="false" />
												<asp:NumericPagerField ButtonType="Link" />
												<asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
											</Fields>
										</asp:DataPager>
									</td>
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
								<tr>
									<tr>
									<th>
										<%--<asp:Label ID="Label5" runat="server" Text="" Width="250px" align="left"> <%# Eval("nazwa_promocji") %>&nbsp; &nbsp; <%# Eval("wysokosc_promocji") %></asp:Label>	--%>
									</th>
								</tr>
									<th>
										<asp:Label ID="Label3" runat="server" Text="" Width="400px" align="center"><h1> <%# Eval("nazwa_produktu") %> </h1></asp:Label>
									</th>
									<th>
										 
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label2" runat="server" Text="ilość :   " Width="200px" align="right"> <%# Eval("ilosc") %></asp:Label>
									</th>
									<th>
										<asp:Label ID="Label4" runat="server" Text="cena :  " Width="200px"> <%# Eval("cena_brutto")%> zł</asp:Label>
									</th>
									
								</tr>
								<tr>
                                    
								</tr>
								<tr>
									<th>
										
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
											
									</th>
								</tr>
								
								<tr>
									
									
								</tr>
						</ItemTemplate>

				</asp:ListView>
			</td>
		</tr>
		<tr>
			<td class="auto-style4">&nbsp;</td>
			<td class="auto-style3">&nbsp;</td>
		</tr>
        <tr>
             <td class="auto-style10" >
                <p>
                <asp:Button class="btn btn-default btn-primary"  ID="ButtonZmien" runat="server" Text="Realizacja zamowienia" OnClick="ZrealizowacZamowienie_Click" />
                </p>
        </tr>
	</table>	

   
    
	
</asp:Content>


