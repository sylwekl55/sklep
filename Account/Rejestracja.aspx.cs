﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class Rejestracja : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonZarejestruj_Click(object sender, EventArgs e)
        {
            RejestracjaKlienta r = new RejestracjaKlienta();

            int nr_lokalu, nr_mieszkania, numer, regon;

            if (! (Int32.TryParse(TextBoxNrLokalu.Text, out nr_lokalu))) //konwertuje na int z stringa 
            {
                labelBlad.Text = "Nieprawidłowy numer lokalu";
                return;
            }

            if (!(Int32.TryParse(TextBoxRegon.Text, out regon)))
            {
                regon = 0;
            }

            if (! (Int32.TryParse(TextBoxNrMieszkania.Text, out nr_mieszkania))) 
            {
                labelBlad.Text = "Nieprawidłowy numer mieszkania";
                return;
            }

            if (! (Int32.TryParse(TextBoxNrTelefonu.Text, out numer)))
            {
                labelBlad.Text = "Nieprawidłowy numer telefonu";
                return;
            }
            
            r.Email = TextBoxEmail.Text;
            r.Haslo = TextBoxHaslo.Text;
            r.Imie = TextBoxImie.Text;
            r.KodPocztowy = TextBoxKodPocztowy.Text;
            r.Kraj = TextBoxKraj.Text;
            r.Login = TextBoxLogin.Text;
            r.Miejscowosc = TextBoxMiejscowosc.Text;
            r.NazwaFirmy = TextBoxNazwaFirmy.Text;
            r.Nazwisko = TextBoxNazwisko.Text;
            r.Nip = TextBoxNIP.Text;
            r.NrLokalu = nr_lokalu;
            r.NrMieszkania = nr_mieszkania;
            r.Numer = numer;
            r.Powiat = TextBoxPowiat.Text;
            r.Regon = TextBoxRegon.Text;
            r.Ulica = TextBoxUlica.Text;
            r.Wojewodztwo = TextBoxWojewudztwo.Text;
            
            try
            {
                if (! r.Rejestruj())
                {
                    labelBlad.Text = "Błąd dodawania klienta: " + r.PowodNieZarejestrowania;
                    return;
                }

                labelBlad.Text = "Zarejestrowano ";
            }
            catch (Exception ex)
            {
                labelBlad.Text = "Błąd dodawania klienta: "+ex.GetType().Name+" ["+ex.Message+"]";
                return;
            }
            // mail aktualizacja

            sklepmaster.baza.Mail m = new sklepmaster.baza.Mail();

            string _from = "sylwekl5@tlen.pl"; //wysyłąnei od nadawcy
            string _to = TextBoxEmail.Text.Trim();//textboxadres.Text.Trim(); wysyłanei do danej osoby
            string _cc = "sylwekl5@tlen.pl";
            string _bcc = "sylwekl5@tlen.pl";
            string _subiect = "Aktywacja adresu e-mail w sklepie z urządzeniami oraz akcesoriami elektornicznymi";
            string _body = File.ReadAllText("C:/Users/Sylwek/Dropbox/inżynierka/sklepmaster/sklepmaster/Account/rwiadomosc.txt"); //textareawiadomosc.InnerText.Trim();//""

            m.wyslijwiadomosc(_from, _to, _cc, _bcc, _subiect, _body, true);


            Response.Redirect("Logowanie.aspx");
        }
    }
}