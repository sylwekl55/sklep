﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class DodajPracownika1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonZarejestruj_Click(object sender, EventArgs e)
        {
            RejestracjaPracownikow r = new RejestracjaPracownikow();

            int nr_lokalu, nr_mieszkania, numer, numer_sluzbowy;

            if (!(Int32.TryParse(TextBoxNrLokalu.Text, out nr_lokalu)))
            {
                labelBlad.Text = "Nie prawidłowy numer lokalu";
                return;
            }

            if (!(Int32.TryParse(TextBoxNrMieszkania.Text, out nr_mieszkania)))
            {
                labelBlad.Text = "Nie prawidłowy numer mieszkania";
                return;
            }

            if (!Int32.TryParse(TextBoxNrTelefonu.Text, out numer))
            {
                labelBlad.Text = "Nie prawidłowy numer telefonu";
                return;
            }
            if (!Int32.TryParse(TextBoxNrSluzbowy.Text, out numer_sluzbowy))
            {
                labelBlad.Text = "Nie prawidłowy numer służbowy";
                return;
            }

            r.Email = TextBoxEmail.Text;
            r.Haslo = TextBoxHaslo.Text;
            r.Imie = TextBoxImie.Text;
            r.KodPocztowy = TextBoxKodPocztowy.Text;
            r.Kraj = TextBoxKraj.Text;
            r.Login = TextBoxLogin.Text;
            r.Miejscowosc = TextBoxMiejscowosc.Text;
            r.NazwaFirmy = TextBoxNazwaFirmy.Text;
            r.Nazwisko = TextBoxNazwisko.Text;
            r.Nip = TextBoxNIP.Text;
            r.NrLokalu = nr_lokalu;
            r.NrMieszkania = nr_mieszkania;
            r.Numer = numer;
            r.Powiat = TextBoxPowiat.Text;
            r.Regon = TextBoxRegon.Text;
            r.Ulica = TextBoxUlica.Text;
            r.Wojewodztwo = TextBoxWojewudztwo.Text;
            r.NumerSluzbowy = numer_sluzbowy; 

            try
            {
                if (! r.Rejestruj())
                {
                    labelBlad.Text = "błąd dodania pracownika" + r.PowodNieZarejestrowania;
                    return;
                }
                labelBlad.Text = "Zarejestrowano ";
            }
            catch (Exception ex)
            {
                labelBlad.Text = "Błąd dodania pracownika: " + ex.GetType().Name + "[" + ex.Message + "]";
                return;
            }
        }
    }
}