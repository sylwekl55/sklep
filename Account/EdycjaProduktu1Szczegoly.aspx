﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EdycjaProduktu1Szczegoly.aspx.cs" Inherits="sklepmaster.Account.EdycjaProduktu1Szczegoly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
		.auto-style1 {
			width: 192px;
		}

		 body 
     {
        margin:0;
        padding:0;
        height:100%; 
        overflow-y:auto;  
     }
     .modal
     {
        display: none; 
        position: absolute;
        top: 0px; 
        left: 0px;
        background-color:black;
        z-index:100;
        opacity: 0.8;
        filter: alpha(opacity=60);
        -moz-opacity:0.8;
        min-height: 100%;
     }
     #divImage
     {
        display: none;
        z-index: 1000;
        position: fixed;
        top: 0;
        left: 0;
        background-color:White;
        height: 550px;
        width: 600px;
        padding: 3px;
        border: solid 1px black;
     }
     * html #divImage {position:absolute;}
		.auto-style2 {
			width: 193px;
		}
		.auto-style3 {
			width: 701px
		}
	</style>


	



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
	
		<div id="divBackground" class="modal">
		</div>
		<div id="divImage" class="info">
			<table style="height: 100%; width: 100%">
				<tr>
					<td >
						<img id="imgLoader" alt=""
							src="images/loader.gif" />
						<img id="imgFull" runat="server" alt="" src=""
							style="display: none; height: 500px; width: 590px" />
						
					</td>
				</tr>
				<tr>
					<td >
						<input id="btnClose" type="button" value="close"
							onclick="HideDiv()" />
					</td>
				</tr>
			</table>

		</div>

	<table>
		<tr>
		
			<td>
				<asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: inherit;">
								<tr>
									<th></th>
								</tr>
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									<td colspan="3">
										<asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
											<Fields>
												<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
													ShowNextPageButton="false" />
												<asp:NumericPagerField ButtonType="Link" />
												<asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
											</Fields>
										</asp:DataPager>
									</td>
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
							<td>

								<%--<asp:ImageButton ID="ImageButton1" Height="113px" Width="150px" runat="server" ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) %> ' Style="cursor: pointer"
									OnClientClick="return LoadDiv(this.src);" />	--%>

                                 <asp:ImageButton Height="113px" Width="150px" runat="server" PostBackUrl='<%# String.Format("StartowaSzczegulyZdjecia.aspx?{0}", Eval("id_fotografii")) %>' ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) %> ' />
									
							</td>
							<%--<td>
									<%# Eval("nazwa_produktu") %>
								</td>
								<td>
									<%# Eval("ilosc_w_magazynie") %>
								</td>
								<td>
									<%# Eval("cena_brutto") %>
								</td>--%>
							<%--<td>
									<%# Eval("opis") %>
								</td>--%>
						</ItemTemplate>

				</asp:ListView>

			</td>
			<td class="auto-style3">
				<asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: 100%;">
								
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									<td colspan="3">
										<asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
											<Fields>
												<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
													ShowNextPageButton="false" />
												<asp:NumericPagerField ButtonType="Link" />
												<asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
											</Fields>
										</asp:DataPager>
									</td>
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
								<tr>
									<tr>
									<th>
										<%-- <asp:Label ID="Label5" runat="server" Text="" Width="250px" align="left"> <%# Eval("promocje") %></asp:Label>	--%>
									</th>
								</tr>
									<th>
										<asp:Label ID="Label3" runat="server" Text="" Width="400px" align="center"><h1> <%# Eval("nazwa_produktu") %> <%# Eval("wersja") %></h1></asp:Label>
									</th>
									
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label2" runat="server" Text="ilość dostępna:   " Width="200px" align="right"> <%# Eval("ilosc_w_magazynie") %></asp:Label>
									</th>
									<th>
										<asp:Label ID="Label4" runat="server" Text="cena :  " Width="200px"> <%# Eval("cena_brutto")%> zł</asp:Label>
									</th>
									
								</tr>
								<tr>

								</tr>
								<tr>
									
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label7" runat="server" Text="" Width="250px" align="left"> <%# Eval("opis") %></asp:Label>
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label8" runat="server" Text="" Width="250px" align="left"> <%# Eval("nazwa_firmy") %></asp:Label>
									</th>
								</tr>
								
								<tr>
									
									
								</tr>
						</ItemTemplate>

				</asp:ListView>

			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			
            
		</tr>
        </table>
    <table>
        <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelNazwaProduktu">Nazwa Produktu</span>
                <asp:TextBox ID="TextBoxNazwaProduktu" runat="server" class="form-control" placeholder="Nazwa Produktu" aria-describedby="labelNazwaProduktu" Style="width: 250px;"></asp:TextBox>
            </div>
		</td>
	</tr>
        <tr>
            <td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelOpis">Opis</span>
                <asp:TextBox   ID="TextBoxOpis"  runat="server" class="form-control" placeholder="Opis" aria-describedby="labelOpis" Style="width: 500px; height: 250px;" TextMode="MultiLine" Wrap="true"   ></asp:TextBox>
            </div>
		    </td>
        </tr>
        <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelCena">Cena</span>
                <asp:TextBox ID="TextBoxCena" runat="server" class="form-control" placeholder="Cena" aria-describedby="labelCena" Style="width: 250px;"></asp:TextBox>
            </div>
		</td>
	</tr>

    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelPromocja">Promocja</span>
                <asp:DropDownList ID="DropDownListPromocja" runat="server" class="form-control" placeholder="Promocja" aria-describedby="labelPromocja" Style="width: 250px;" AppendDataBoundItems="True"></asp:DropDownList>
            </div>
		</td>
	</tr>
    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelIloscWMagazynie">Ilość w magazynie</span>
                <asp:TextBox ID="TextBoxIloscWMagazynie" runat="server" class="form-control" placeholder="Ilość w magazynie" aria-describedby="labelIloscWMagazynie" Style="width: 250px; top: 0px; left: 0px;"></asp:TextBox>
            </div>
		</td>
	</tr>
       <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelKategoria">Kategoria</span>
                <asp:DropDownList ID="DropDownListKategoria" runat="server" class="form-control" placeholder="Kategoria" aria-describedby="labelKategoria" Style="width: 250px;" AppendDataBoundItems ="true"></asp:DropDownList>
            </div>
		</td>
	</tr>
    <tr>
            <td class="auto-style3">
                <asp:Button class="btn btn-default btn-primary" ID="ButtonEdytuj" runat="server" Text="Zmień" OnClick="ButtonEdytuj_Click" />
            </td>
    </tr>
    </table>	


    <asp:Label ID="labelBlad" runat="server" Text="Label"></asp:Label>
	
</asp:Content>
