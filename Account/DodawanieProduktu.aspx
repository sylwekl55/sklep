﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DodawanieProduktu.aspx.cs" Inherits="sklepmaster.Account.DodawanieProduktu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
	.auto-style1 {
		width: 100%;
	}
	.auto-style2 {
		width: 424px;
	}
	.auto-style3 {
		width: 245px;
	}
		.auto-style4 {
			width: 245px;
			height: 24px;
		}
		.auto-style5 {
			height: 24px;
		}
		.auto-style6 {
			width: 245px;
			height: 29px;
		}
		.auto-style7 {
			height: 29px;
		}
        .tabele {
            border-radius: 16px;
            border="1" cellspacing="10";
        }

         .table-produkty{
            border-radius: 16px;
        }
        .table-produkty td{
            vertical-align: middle !important;/*important aby inne style nie nadpisywały*/
        }
        .table-produkty th{
            padding: 0px !important;
        }
        .table-produkty input{
            width: 100%;
            height: 84px;
            border:none;
        }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1 table" style="height: inherit;">
	<tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelNazwaProduktu">Nazwa Produktu</span>
                <asp:TextBox ID="TextBoxNazwaProduktu" runat="server" class="form-control" placeholder="Nazwa Produktu" aria-describedby="labelNazwaProduktu" Style="width: 250px;"></asp:TextBox>
            </div>
		</td>
	</tr>
	<tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelIloscWMagazynie">Ilość w magazynie</span>
                <asp:TextBox ID="TextBoxIloscWMagazynie" runat="server" class="form-control" placeholder="Ilość w magazynie" aria-describedby="labelIloscWMagazynie" Style="width: 250px; top: 0px; left: 0px;"></asp:TextBox>
            </div>
		</td>
	</tr>
	<tr>
		<td class="auto-style11">
            <div class="input-group">
                <span  style="width: 200px;" class="input-group-addon" id="labelWersja">Wersja</span>
                <asp:TextBox ID="TextBoxWersja" runat="server" class="form-control" placeholder="Wersja" aria-describedby="labelWersja" Style="width: 250px; top: 0px; left: 0px;"></asp:TextBox>
            </div>
		</td>
	</tr>
	<tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelOpis">Opis</span>
                <asp:TextBox ID="TextBoxOpis" runat="server" class="form-control" placeholder="Opis" aria-describedby="labelOpis" style="width: 250px; height:100px"></asp:TextBox>
            </div>
		</td>
        
	</tr>
	<tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelCena">Cena</span>
                <asp:TextBox ID="TextBoxCena" runat="server" class="form-control" placeholder="Cena" aria-describedby="labelCena" Style="width: 250px;"></asp:TextBox>
            </div>
		</td>
	</tr>
    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelVat">Vat</span>
                <asp:DropDownList ID="DropDownListVat" runat="server" class="form-control" placeholder="Vat" aria-describedby="labelVat" Style="width: 250px;"></asp:DropDownList>
            </div>
		</td>
	</tr>
    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelProducent">Producent</span>
                <asp:DropDownList ID="DropDownListProducent" runat="server" class="form-control" placeholder="Vat" aria-describedby="labelProducent" Style="width: 250px;"></asp:DropDownList>
            </div>
		</td>
        <td class="auto-style11">
            <asp:Button class="btn btn-info btn-lg" ID="buttonWyloguj" runat="server" Text="Dodaj Producenta" OnClick="buttonDodajProducenta_Click" />
        </td>
	</tr>
    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelKategoria">Kategoria</span>
                <asp:DropDownList ID="DropDownListKategoria" runat="server" class="form-control" placeholder="Kategoria" aria-describedby="labelKategoria" Style="width: 250px;" TextMode="MultiLine" Wrap="true" ></asp:DropDownList>
            </div>
		</td>
	</tr>
        
    <tr>
		<td class="auto-style11">
            <div class="input-group">
                <span style="width: 200px;" class="input-group-addon" id="labelPromocja">Promocja</span>
                <asp:DropDownList ID="DropDownListPromocja" runat="server" class="form-control" placeholder="Promocja" aria-describedby="labelPromocja" Style="width: 250px;"></asp:DropDownList>
            </div>
		</td>
	</tr>

	<tr>
		<td class="auto-style3">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style3">dodaj zdjęcie produktu</td>
		<td>
			<asp:FileUpload ID="FileUpload1" runat="server" />
		</td>

	</tr>
	<tr>
		<td class="auto-style11">
				<div class="input-group">
					<%--<span style="width: 200px;" class="input-group-addon" id="labelOpisZdjecia">Opis Zdjecia</span>
					<asp:TextBox ID="TextBoxOpisZdjecia" runat="server" class="form-control" placeholder="Opis Zdjecia" aria-describedby="labelOpisZdjecia" style="width: 250px; height:100px" TextMode="MultiLine" OnTextChanged="TextBoxOpisZdjecia_TextChanged"></asp:TextBox>
				--%></div>
		</td> 
	</tr>
	<tr>
		<td>
			<asp:Button class="btn btn-default btn-primary" ID="ButtonDodaj" runat="server" Text="Dodaj" OnClick="ButtonDodaj_Click"/>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style3">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="auto-style3">&nbsp;<asp:Label ID="LabelBlad" runat="server" Text=""></asp:Label></td>
		<td>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </td>
	</tr>
</table>
</asp:Content>
