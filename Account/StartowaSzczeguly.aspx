﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StartowaSzczeguly.aspx.cs" Inherits="sklepmaster.Account.StartowaSzczeguly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		.auto-style1 {
			width: 192px;
		}

		 body 
     {
        margin:0;
        padding:0;
        height:100%; 
        overflow-y:auto;  
     }
     .modal
     {
        display: none; 
        position: absolute;
        top: 0px; 
        left: 0px;
        background-color:black;
        z-index:100;
        opacity: 0.8;
        filter: alpha(opacity=60);
        -moz-opacity:0.8;
        min-height: 100%;
     }
     #divImage
     {
        display: none;
        z-index: 1000;
        position: fixed;
        top: 0;
        left: 0;
        background-color:White;
        height: 550px;
        width: 600px;
        padding: 3px;
        border: solid 1px black;
     }
     * html #divImage {position:absolute;}
		.auto-style2 {
			width: 193px;
		}
		.auto-style3 {
			width: 701px
		}
    .bezBorder{
        border: none !important;
    }
    .bezBorder tr{
        border: none !important;
    }
    .bezBorder th{
        border: none !important;
    }
    .bezBorder td{
        border: none !important;
    }
	</style>


	



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	
	
		<div id="divBackground" class="modal">
		</div>
		<div id="divImage" class="info">
			<table style="height: 100%; width: 100%" class="table bezBorder">
				<tr>
					<td >
						<img id="imgLoader" alt=""
							src="images/loader.gif" />
						<img id="imgFull" runat="server" alt="" src=""
							style="display: none; height: 500px; width: 590px" />
						
					</td>
				</tr>
				<tr>
					<td >
						<input id="btnClose" type="button" value="close"
							onclick="HideDiv()" />
					</td>
				</tr>
			</table>


		</div>

	<table class="table bezBorder">
		<tr>
		
			<td>
				<asp:ListView ID="lvCustomers" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: inherit;" class="table bezBorder">
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									<td colspan="3">
										<asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
											<Fields>
												<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
													ShowNextPageButton="false" />
												<asp:NumericPagerField ButtonType="Link" />
												<asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
											</Fields>
										</asp:DataPager>
									</td>
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
							<td>

								<%--<asp:ImageButton ID="ImageButton1" Height="113px" Width="150px" runat="server" ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) %> ' Style="cursor: pointer"
									OnClientClick="return LoadDiv(this.src);" />	--%>

                                 <asp:ImageButton Height="113px" Width="150px" runat="server" PostBackUrl='<%# String.Format("StartowaSzczegulyZdjecia.aspx?{0}", Eval("id_fotografii")) %>' ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("zdjecie")) %> ' />
									
							</td>
							<%--<td>
									<%# Eval("nazwa_produktu") %>
								</td>
								<td>
									<%# Eval("ilosc_w_magazynie") %>
								</td>
								<td>
									<%# Eval("cena_brutto") %>
								</td>--%>
							<%--<td>
									<%# Eval("opis") %>
								</td>--%>
						</ItemTemplate>

				</asp:ListView>

			</td>
			<td class="auto-style3">
				<asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder2" >

					<LayoutTemplate>
							<table border="0" style="width: 100%; height: 100%;">
								
								<asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
								<tr>
									<td colspan="3">
										<asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvCustomers" PageSize="10">
											<Fields>
												<asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
													ShowNextPageButton="false" />
												<asp:NumericPagerField ButtonType="Link" />
												<asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
											</Fields>
										</asp:DataPager>
									</td>
								</tr>
							</table>
						</LayoutTemplate>
						<GroupTemplate>
							<tr>
								<asp:PlaceHolder runat="server" ID="itemPlaceHolder2"></asp:PlaceHolder>
							</tr>
						</GroupTemplate>
						<ItemTemplate>
								<tr>
									<tr>
									<th>
										<%--<asp:Label ID="Label5" runat="server" Text="" Width="250px" align="left"> <%# Eval("nazwa_promocji") %>&nbsp; &nbsp; <%# Eval("wysokosc_promocji") %></asp:Label>	--%>
									</th>
								</tr>
									<th>
										<asp:Label ID="Label3" runat="server" Text="" Width="400px" align="center"><h1> <%# Eval("nazwa_produktu") %> </h1></asp:Label>
									</th>
									<th>
										 <asp:LinkButton ID="DodajDoKoszyka" runat="server" PostBackUrl='<%# String.Format("Koszyk.aspx?{0}", Eval("id_produktu")) %>' Width="150px" align="right">Dodaj do koszyka</asp:LinkButton>
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label2" runat="server" Text="ilość dostępna:   " Width="200px" align="right"> <%# Eval("ilosc_w_magazynie") %></asp:Label>
									</th>
									<th>
										<asp:Label ID="Label4" runat="server" Text="cena :  " Width="200px"> <%# Eval("cena_brutto")%> zł</asp:Label>
									</th>
									
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label7" runat="server" Text="" Width="250px" align="left"> <%# Eval("opis") %></asp:Label>	
									</th>
								</tr>
								<tr>

								</tr>
								<tr>
									<th>
										<asp:Label ID="Label8" runat="server" Text="" Width="250px" align="left"> <%# Eval("nazwa_firmy") %></asp:Label>	
									</th>
								</tr>
								
								<tr>
									
									
								</tr>
						</ItemTemplate>

				</asp:ListView>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">&nbsp;</td>
		</tr>
	</table>	



	
</asp:Content>
