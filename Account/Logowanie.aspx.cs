﻿using sklepmaster.baza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster.Account
{
    public partial class Logowanie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonZaloguj_Click(object sender, EventArgs e)
        {
            sklepmaster.baza.Logowanie logowanie = new sklepmaster.baza.Logowanie();

            try
            {
                if (! logowanie.Zaloguj(TextBoxLogin.Text, TextBoxHaslo.Text))
                {
                    labelBlad.Text = logowanie.PowodNiezalogowania;
                    return;
                }
                DaneZalogowanejOsoby dane = (DaneZalogowanejOsoby)Session["klient"];

                
                DaneZalogowanejOsoby Dane = new DaneZalogowanejOsoby();

                Session["klient"] = logowanie.Dane;
                Session["Login"] = TextBoxLogin.Text;
                Session["IdOsoba"] = logowanie.Dane.IdOsoba;
                Session["Imie"] = logowanie.Dane.Imie;
                Session["Nazwisko"] = logowanie.Dane.Nazwisko;
                Session["Pracownik"] = logowanie.Dane.Pracownik;
                Session["NazwaFirmy"] = logowanie.Dane.NazwaFirmy;
                Session["Nip"] = logowanie.Dane.Nip;
                Session["Regon"] = logowanie.Dane.Regon;

                Session["IdAdres"] = logowanie.Dane.IdAdres;
                Session["Ulica"] = logowanie.Dane.Ulica;
                Session["NrLokalu"] = logowanie.Dane.NrLokalu;
                Session["NrMieszkania"] = logowanie.Dane.NrMieszkania;
                Session["KodPocztowy"] = logowanie.Dane.KodPocztowy;
                Session["Miejscowowsc"] = logowanie.Dane.Miejscowowsc;
                Session["Powiat"] = logowanie.Dane.Powiat;
                Session["Wojewodztwo"] = logowanie.Dane.Wojewodztwo;
                Session["Kraj"] = logowanie.Dane.Kraj;
                Session["NrTelefonu"] = logowanie.Dane.NrTelefonu;
                Session["Nip"] = logowanie.Dane.Nip;
               
                Page.Response.Redirect("/Account/Starowa.aspx");
                labelBlad.Text = "Zalogowano: " + logowanie.Dane.Imie + " " + logowanie.Dane.Nazwisko;
            }
            catch (Exception ex)
            {
                labelBlad.Text = "Błąd: " + ex.GetType().Name + " " + ex.Message;
            }
            //Response.Redirect("/Account/Starowa.aspx");
        }

        protected void buttonRejestracja_Click(object sender, EventArgs e)
        {
            Page.Response.BufferOutput = true;
            Page.Response.Redirect("/Account/Rejestracja.aspx");
        }
    }
}