﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza
{
    public class DaneZalogowanejOsoby
    {
        public int IdKlienta { get; set; }
        public int IdOsoba { get; set; }
        public int IdAdres { get; set; }
        public String Login { get; set; }
        public String Imie { get; set; }
        public String Nazwisko { get; set; }
        public String NazwaFirmy { get; set; }
        public String Regon { get; set; }
        public String Nip { get; set; }
        public bool Pracownik { get; set; }
        public int IdPracownik { get; set; }
        public bool DodajePracownikow { get; set; }
        public String Ulica { get; set; }
        public int NrLokalu{ get; set; }
        public int NrMieszkania{ get; set; }
        public string KodPocztowy{ get; set; }
        public string Miejscowowsc { get; set; }
        public string Powiat { get; set; }
        public string Wojewodztwo { get; set; }
        public string Kraj { get; set; }
        public int NrTelefonu { get; set; }

        public static bool CzyAdmin(object obj) // sprawdzam czy jest zalogowany czy to jest admin
        {
            if(obj==null || (!(obj is DaneZalogowanejOsoby)))
            {
                return false;
            }
            DaneZalogowanejOsoby o = (DaneZalogowanejOsoby)obj;
            if(o.Pracownik && o.DodajePracownikow)
            {
                return true;
            }
            return false;
        }

        public static bool CzyZalogowany(object obj) // czy ktokolwiek jest zalogowany
        {
            if (obj == null || (!(obj is DaneZalogowanejOsoby)))
            {
                return false;
            }

            return true;
        }
    }
}