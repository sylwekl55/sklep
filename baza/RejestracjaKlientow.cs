﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sklepmaster.baza.pola;
using System.Data;

namespace sklepmaster
{
	public class RejestracjaKlienta
	{
        public int IdOsoba { get; set; }
        public int IdAdres { get; set; }
		public int NrLokalu { get; set; }
		public String Kraj { get; set; }
		public String KodPocztowy { get; set; }
		public String Powiat { get; set; }
		public String Wojewodztwo { get; set; }
		public int NrMieszkania { get; set; }
		public String Ulica { get; set; }
		public String Email { get; set; }

		/// <summary>
		/// Numer telefonu
		/// </summary>
		public int Numer { get; set; }

		public String Miejscowosc { get; set; }

		public String Imie { get; set; }
		public String Nazwisko { get; set; }
		public String Login { get; set; }
		public String Haslo { get; set; }

		public String NazwaFirmy { get; set; }
		public String Regon { get; set; }
		public String Nip { get; set; }
		public String PowodNieZarejestrowania { get; private set; }
        public bool CzyZmianaHasla { get; set; }

		public bool Rejestruj()
		{

			try
			{
				using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
					// Otwórz połączenie
					polaczenie.Open();

                   if (!Osoba.SprawdzLogin(polaczenie, Login))
                   {
                       PowodNieZarejestrowania = "Podany login jest już zajęty";

                       return false;
                   }

					long id_adres = Adres.DodajAdres(polaczenie,
						NrLokalu,
						Kraj,
						KodPocztowy,
						Powiat,
						Wojewodztwo,
						NrMieszkania,
						Ulica,
						Email,
						Numer,
						Miejscowosc);
					long id_osoba = Osoba.DodajOsoba(polaczenie,
						Imie,
						Nazwisko,
						Login,
						Haslo,
						false,
						id_adres);
					_DodajKlient(polaczenie, id_osoba);
					return true;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
			}
		}

		private void _DodajKlient(MySqlConnection polaczenie, long id_osoba)
		{
			// Compose query using sql parameters
			string polecenie_sql = "INSERT INTO " + Klient.NazwaTabeli +
				" (" + Klient.WszystkieNazwyPol +
				") values (" + Klient.WszystkieNazwyParametrow + ")";

			using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
			{
                komenda.Parameters.AddWithValue(Klient.ParamOsoba, id_osoba);
				komenda.Parameters.AddWithValue(Klient.ParamNazwaFirmy, NazwaFirmy);
				komenda.Parameters.AddWithValue(Klient.ParamRegon, Regon);
				komenda.Parameters.AddWithValue(Klient.ParamNip, Nip);

				komenda.ExecuteNonQuery();
			}
		}
        public bool Edytuj()
        {
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();

                    long id_adres = Adres.EdytujAdres(polaczenie,
                        NrLokalu,
                        Kraj,
                        KodPocztowy,
                        Powiat,
                        Wojewodztwo,
                        NrMieszkania,
                        Ulica,
                        Numer,
                        Miejscowosc,
                        IdAdres);
                    if (CzyZmianaHasla == true)
                    {
                        long id_osoba = Osoba.EdytujOsoba(polaczenie,
                        Haslo,
                        Login);
                    }                   
                    _EdytujKlienta(polaczenie);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }

        }

        private void _EdytujKlienta(MySqlConnection polaczenie)
        {
            string polecenie_sql = "UPDATE " + Klient.NazwaTabeli +
                  " SET " + Klient.NazwaFirmy + "=" + Klient.ParamNazwaFirmy + "," +
                  Klient.Regon + "=" + Klient.ParamRegon + "," +
                  Klient.Nip + "=" + Klient.ParamNip +
                  " WHERE " + Klient.Osoba + "=" + Klient.ParamOsoba;

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(Klient.ParamOsoba, IdOsoba);
                komenda.Parameters.AddWithValue(Klient.ParamNazwaFirmy, NazwaFirmy);
                komenda.Parameters.AddWithValue(Klient.ParamRegon, Regon);
                komenda.Parameters.AddWithValue(Klient.ParamNip, Nip);

                komenda.ExecuteNonQuery();
            }
        }
	}
}