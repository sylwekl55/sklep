﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza
{
    public class DaneProduktu
    {
        public int IdProduktu { get; set; }

        public String NazwaFirmy { get; set; }
        public String NazwaProduktu {get; set;}
        public int IloscWMagazynie { get; set; }
        public String Wersja { get; set; }
        public String Opis { get; set; }
        public float CenaBrutto { get; set; }
        public int Ocena { get; set; }
        public int Producent { get; set; }
        public int Kategoria { get; set; }
        public int Vat { get; set; }
        public int Promocje { get; set; }
        public String PowodNieZarejestrowania { set; get; }


        public bool Edytuj()
        {
            try
            {
                using(MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    _EdytujProdukt(polaczenie);
                
                }
            }
            catch (Exception ex)
            {
                PowodNieZarejestrowania = ex.Message;
                throw new Exception("Cannot update record [" + ex.Message + "]", ex);
            }
            return true;
        }

        private void _EdytujProdukt(MySqlConnection polaczenie)
        {
            string polecenie_sql = "UPDATE " + sklepmaster.baza.pola.Produkt.NazwaTabeli +
                  " SET " + sklepmaster.baza.pola.Produkt.Opis + "=" + sklepmaster.baza.pola.Produkt.ParamOpis + "," +
                  sklepmaster.baza.pola.Produkt.CenaBrutto + "=" + sklepmaster.baza.pola.Produkt.ParamCenaBrutto + "," +
                  sklepmaster.baza.pola.Produkt.IloscWMagazynie + "=" + sklepmaster.baza.pola.Produkt.ParamIloscWMagazynie + "," +
                  sklepmaster.baza.pola.Produkt.Promocje + "=" + sklepmaster.baza.pola.Produkt.ParamPromocje + "," +
                  sklepmaster.baza.pola.Produkt.Kategoria+ "=" + sklepmaster.baza.pola.Produkt.ParamKategoria +
                  " WHERE " + sklepmaster.baza.pola.Produkt.IdProduktu + "=" + sklepmaster.baza.pola.Produkt.ParamIdProduktu;

                using(MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
                {
                    
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamOpis, Opis);
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamCenaBrutto, CenaBrutto);
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamIloscWMagazynie, IloscWMagazynie);
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamPromocje, Promocje);
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamKategoria, Kategoria);
                    komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamIdProduktu, IdProduktu);

                    komenda.ExecuteNonQuery();
                }
        }

        private void _EdytujProdukt111(MySqlConnection polaczenie)
        {
            
        }
    }
}