﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class Kup
    {
        public int id_produktu { get; set; }
        public int ilosc_odjeta { get; set; }
        public int ilosc_w_magazynie { get; set; }
        public int ilosc_w_magazynie_zastap { get; set; }
        public string PowodNieZarejestrowania { get; set; }
        internal void Odejmij()
        {
            ilosc_w_magazynie_zastap = ilosc_w_magazynie - ilosc_odjeta;
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    _EdytujProdukt(polaczenie);

                }
            }
            catch (Exception ex)
            {
                PowodNieZarejestrowania = ex.Message;
                throw new Exception("Cannot update record [" + ex.Message + "]", ex);
            }
            
        }

        private void _EdytujProdukt(MySqlConnection polaczenie)
        {
            string polecenie_sql = "UPDATE " + sklepmaster.baza.pola.Produkt.NazwaTabeli +
                 " SET " + 
                 sklepmaster.baza.pola.Produkt.IloscWMagazynie + "=" + sklepmaster.baza.pola.Produkt.ParamIloscWMagazynie + 
                 " WHERE " + sklepmaster.baza.pola.Produkt.IdProduktu + "=" + sklepmaster.baza.pola.Produkt.ParamIdProduktu;

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamIloscWMagazynie, ilosc_w_magazynie_zastap);
                komenda.Parameters.AddWithValue(sklepmaster.baza.pola.Produkt.ParamIdProduktu, id_produktu);

                komenda.ExecuteNonQuery();
            }
        }
    }
}