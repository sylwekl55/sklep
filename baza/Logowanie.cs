﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sklepmaster.baza.pola;
using System.Data;

namespace sklepmaster.baza
{
    public class Logowanie
    {
        public DaneZalogowanejOsoby Dane { get; private set; }

        public String PowodNiezalogowania;

        public Logowanie()
        {
            Dane = new DaneZalogowanejOsoby();
        }

        public bool Zaloguj(String login, String haslo)
        {
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();

                    if (!_PobierzDaneOsoby(polaczenie, login, haslo))
                    {
                        return false;
                    }
                    if (Dane.Pracownik)
                    {
                        _PobierzDanePracownika(polaczenie);
                    }
                    else
                    {
                        _PobierzDaneKlienta(polaczenie);
                    }
                    if (!_PobierzDaneAdresu(polaczenie, login))
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool _PobierzDaneOsoby(MySqlConnection polaczenie, String login, String haslo)
        {
            string polecenie_sql = "SELECT " + Osoba.IdOsoba + ',' + Osoba.Imie + ',' + Osoba.Nazwisko + ',' + Osoba.Login + ',' + Osoba.Haslo  + ',' + Osoba.Pracownik +
                " FROM " + Osoba.NazwaTabeli + " WHERE " + Osoba.Login + "=" + Osoba.ParamLogin;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Osoba.ParamLogin, login);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count == 0)
            {
                PowodNiezalogowania = "Podany login nie istnieje";
                return false;
            }

            DataRow rekord = tabela.Rows[0];

            String haslo_z_bazy = (String)rekord[Osoba.Haslo];

            if (!haslo.Equals(haslo_z_bazy))
            {
                PowodNiezalogowania = "Błędne hasło";
                return false;
            }

            Dane.IdOsoba = (int)rekord[Osoba.IdOsoba];
            Dane.Imie = (String)rekord[Osoba.Imie];
            Dane.Login = (String)rekord[Osoba.Login];
            Dane.Nazwisko = (String)rekord[Osoba.Nazwisko];
            Dane.Pracownik = (bool)rekord[Osoba.Pracownik];

            return true;
        }

        public void _PobierzDaneKlienta(MySqlConnection polaczenie)
        {
            string polecenie_sql = "SELECT " + Klient.IdKlient + ',' + Klient.NazwaFirmy + ',' + Klient.Regon + ',' + Klient.Nip +
                " FROM " + Klient.NazwaTabeli + " WHERE " + Klient.Osoba + "=" + Klient.ParamOsoba;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Klient.ParamOsoba, Dane.IdOsoba);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count == 0)
            {
                throw new Exception("Nie znaleziono klienta dla " + Klient.Osoba + "=" + Dane.IdOsoba);
            }

            DataRow rekord = tabela.Rows[0];

            Dane.IdKlienta = (int)rekord[Klient.IdKlient];
            Dane.NazwaFirmy = (String)rekord[Klient.NazwaFirmy];
            Dane.Regon = (String)rekord[Klient.Regon];
            Dane.Nip = (String)rekord[Klient.Nip];
            Dane.Pracownik = false;
        }

        public void _PobierzDanePracownika(MySqlConnection polaczenie)
        {
            string polecenie_sql = "SELECT " + Pracownik.IdPracownika + ',' + Pracownik.DodajePracownikow +
                " FROM " + Pracownik.NazwaTabeli + " WHERE " + Pracownik.Osoba + "=" + Pracownik.ParamOsoba;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Pracownik.ParamOsoba, Dane.IdOsoba);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count == 0)
            {
                throw new Exception("Nie znaleziono Pracownika dla " + Pracownik.Osoba + "=" + Dane.IdOsoba);
            }

            DataRow rekord = tabela.Rows[0];

            Dane.IdPracownik = (int)rekord[Pracownik.IdPracownika];
            Dane.DodajePracownikow = (bool)rekord[Pracownik.DodajePracownikow];
            Dane.Pracownik = true;
          
        }
        public bool _PobierzDaneAdresu(MySqlConnection polaczenie, String login)
        {
            string polecenie_sql = "SELECT " + Adres.IdAdres + ',' + Adres.Ulica + ',' + Adres.NrLokalu + ',' + Adres.NrMieszkania + ',' + Adres.KodPocztowy + ',' + Adres.Miejscowosc + ',' + Adres.Powiat + ',' + Adres.Wojewodztwo + ',' + Adres.Kraj + ',' + Adres.Numer +
            " from " + Adres.NazwaTabeli + ',' + Osoba.NazwaTabeli + " where " + Adres.IdAdres + " = "+ Osoba.Adres + " AND " + Osoba.Login + " = " + Osoba.ParamLogin + ";";

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Osoba.ParamLogin, login);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count == 0)
            {
                PowodNiezalogowania = "Podany login nie istnieje";
                
            }

            DataRow rekord = tabela.Rows[0];
            Dane.IdAdres = (int)rekord[Adres.IdAdres];
            Dane.Ulica = (string)rekord[Adres.Ulica];
            Dane.NrLokalu = (int)rekord[Adres.NrLokalu];
            Dane.NrMieszkania = (int)rekord[Adres.NrMieszkania];
            Dane.KodPocztowy = (String)rekord[Adres.KodPocztowy];
            Dane.Miejscowowsc = (String)rekord[Adres.Miejscowosc];
            Dane.Powiat = (String)rekord[Adres.Powiat];
            Dane.Wojewodztwo = (String)rekord[Adres.Wojewodztwo];
            Dane.Kraj = (String)rekord[Adres.Kraj];
            Dane.NrTelefonu = (int)rekord[Adres.Numer];

            return true;
        }
    }
}