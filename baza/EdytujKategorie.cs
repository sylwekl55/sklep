﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza
{
    public class EdytujKategorie
    {
        public string Nazwa { get; set; }
        public int Bazowa { get; set; }
        public String PowodNieUdanejEdycji { get; private set; }


        public bool EdytownieKategorii()
        {
            try
            {
                using (MySql.Data.MySqlClient.MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    bool id_kategoria = pola.PolaKategoria.EdytujKategorie(polaczenie, Bazowa, Nazwa);

                    //if (!sklepmaster.baza.pola.PolaKategoria.EdytujKategorie(polaczenie, Bazowa, Nazwa))
                   // {
                    //    PowodNieUdanejEdycji = "edycja danej kategorii jest niemożliwa  ";
                     //   return false;
                    //}
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot update record [" + ex.Message + "]", ex);
            }
        }
    }
}