﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza
{
	public class DodawanieProducentow
	{
		public String NazwaFirmy { get; set; }
		public int Nip { get; set; }
		public int Regon { get; set; }
		public string PowodNieZarejestrowania { get; private set; }

		public int NrLokalu { get; set; }
		public String Kraj { get; set; }
		public String KodPocztowy { get; set; }
		public String Powiat { get; set; }
		public String Wojewodztwo { get; set; }
		public int NrMieszkania { get; set; }
		public String Ulica { get; set; }
		public String Email { get; set; }
		public int Numer { get; set; }
		public String Miejscowosc { get; set; }

		public bool RejestrujProducenta()
		{
			try
			{
				using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
					polaczenie.Open();
					long id_adres = sklepmaster.baza.pola.Adres.DodajAdres(polaczenie,
						NrLokalu,
						Kraj,
						KodPocztowy,
						Powiat,
						Wojewodztwo,
						NrMieszkania,
						Ulica,
						Email,
						Numer,
						Miejscowosc);

					long id_producent = sklepmaster.baza.pola.Producent.DodajProducentaa(polaczenie,
						NazwaFirmy,
						Nip,
						Regon,
						id_adres
						);
					return true;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
			}
		}
			
	}
}