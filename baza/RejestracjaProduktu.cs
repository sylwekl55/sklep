﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class RejestracjaProduktu
    {
        public String NazwaProduktu { get; set; }
        public int IloscwMagazynie { get; set; }
        public String Wersja { get; set; }
        public String Opis { get; set; }
        public float CenaBrutto { get; set; }
        public int Producent { get; set; }
        public int Vat { get; set; }
        public int Promocje { get; set; }
        public int Kategoria { get; set; }
        public String PowodNieZarejestrowania { get; private set; }

        public bool Rejestruj()
        {
            try
            {
                using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    // Otwórz połączenie
                    polaczenie.Open();

                    //if (!Produkt.SprawdzProdukt(polaczenie, NazwaProduktu, Wersja))
                    //{
                    //    PowodNieZarejestrowania = "Produkt o tej nazwie i modelu istnieje ";

                    //    return false;
                    //}

                    long id_produktu = Produkt.DodajProdukt(polaczenie,
                        NazwaProduktu,
                        IloscwMagazynie,
                        Wersja,
                        Opis,
                        CenaBrutto,
                        Producent,
                        Vat,
                        Promocje,
                        Kategoria);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw new Exception("nie stworzony insert  [" + ex.Message + "]", ex);
            }
        }
    }
}