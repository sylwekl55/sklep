﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sklepmaster.baza.pola;

namespace sklepmaster.baza
{
    public class RejestracjaKategorii
    {
        public int Bazowa { get; set; }
        public String Nazwa { get; set; }
        public String PowodNieZarejestrowania { get; private set; }

        public bool RejestrujKategorie()
        {
            try
            {
                using (MySql.Data.MySqlClient.MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    long id_kategoria = sklepmaster.baza.pola.PolaKategoria.DodajKategorie(polaczenie,
                        Nazwa,
                        Bazowa);
                    return true;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
        }

        public bool RejestrujKategorieBazowe()
        {
            try
            {
                using (MySql.Data.MySqlClient.MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    long id_kategoria = sklepmaster.baza.pola.PolaKategoria.DodajKategorieBazowe(polaczenie,
                        Nazwa);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
        }
    }
}