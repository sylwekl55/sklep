﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class Zamowienie
    {
        public static readonly String NazwaTabeli = "Zamowienie";

        public static readonly String IdZamowienia = "id_zamowienie";

        public static readonly String DataZamowienia = "data_zamowienia";
        public static readonly String CzyPrzyjentoZamuwienie = "czy_przyjeto_zamowienie";
        public static readonly String Zaplacono = "zaplacono";
        public static readonly String DataWysylki = "data_wysylki";
        public static readonly String CzyZamowienieZrealizowano = "czy_zamowienie_zrealizowano";
        public static readonly String DataZrealizowaniaZamowienia = "data_zrealizowania_zamowienia";
        public static readonly String SposobPlatnosci = "sposob_platnosci";
        public static readonly String Klient = "klient";
        public static readonly String Adres = "adres";
        public static readonly String Kurier = "kurier";
        public static readonly String Pracownik = "pracownik";
        



    }
}