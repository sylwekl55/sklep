﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public static class Adres
    {
        public static readonly String NazwaTabeli = "Adres";

        public static readonly String IdAdres = "id_adresu"; 

        public static readonly String NrLokalu = "nr_lokalu";
        public static readonly String Kraj = "kraj";
        public static readonly String KodPocztowy = "kod_pocztowy";
        public static readonly String Powiat = "powiat";
        public static readonly String Wojewodztwo = "wojewodztwo";
        public static readonly String NrMieszkania = "nr_mieszkania";
        public static readonly String Ulica = "ulica";
        public static readonly String Email = "email";
        public static readonly String Numer = "numer";
        public static readonly String Miejscowosc = "miejscowosc";

        public static readonly String ParamIdAdres = '@' + IdAdres;
        public static readonly String ParamNrLokalu = '@' + NrLokalu;
        public static readonly String ParamKraj = '@' + Kraj;
        public static readonly String ParamKodPocztowy = '@' + KodPocztowy;
        public static readonly String ParamPowiat = '@' + Powiat;
        public static readonly String ParamWojewodztwo = '@' + Wojewodztwo;
        public static readonly String ParamNrMieszkania = '@' + NrMieszkania;
        public static readonly String ParamUlica = '@' + Ulica;
        public static readonly String ParamEmail = '@' + Email;
        public static readonly String ParamNumer = '@' + Numer;
        public static readonly String ParamMiejscowosc = '@' + Miejscowosc;
       

        public static readonly String WszystkieNazwyPol =
            NrLokalu + ',' +
            Kraj + ',' +
            KodPocztowy + ',' +
            Powiat + ',' +
            Wojewodztwo + ',' +
            NrMieszkania + ',' +
            Ulica + ',' +
            Email + ',' +
            Numer + ',' +
            Miejscowosc;

        public static readonly String WszystkieNazwyParametrow =
            ParamNrLokalu + ',' +
            ParamKraj + ',' +
            ParamKodPocztowy + ',' +
            ParamPowiat + ',' +
            ParamWojewodztwo + ',' +
            ParamNrMieszkania + ',' +
            ParamUlica + ',' +
            ParamEmail + ',' +
            ParamNumer + ',' +
            ParamMiejscowosc;

        public static long DodajAdres(MySqlConnection polaczenie,
            int nrLokalu,
            string kraj,
            string kodPocztowy,
			string powiat,
			string wojewodztwo,
			int nrMieszkania,
			string ulica,
			string email,
			int numer,
			string miejscowosc)
        {
            // Compose query using sql parameters
            string polecenie_sql = "INSERT INTO " + NazwaTabeli +
                " (" + WszystkieNazwyPol + ") " +
                "values (" + WszystkieNazwyParametrow + ")";

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
				komenda.Parameters.AddWithValue(Adres.ParamNrLokalu, nrLokalu);  //AddWithValue --Dodaje wartość do końca SqlParameterCollection.
				komenda.Parameters.AddWithValue(Adres.ParamKraj, kraj);
				komenda.Parameters.AddWithValue(Adres.ParamKodPocztowy, kodPocztowy);
				komenda.Parameters.AddWithValue(Adres.ParamPowiat, powiat);
				komenda.Parameters.AddWithValue(Adres.ParamWojewodztwo, wojewodztwo);
				komenda.Parameters.AddWithValue(Adres.ParamNrMieszkania, nrMieszkania);
				komenda.Parameters.AddWithValue(Adres.ParamUlica, ulica);
				komenda.Parameters.AddWithValue(Adres.ParamEmail, email);
				komenda.Parameters.AddWithValue(Adres.ParamNumer, numer);
				komenda.Parameters.AddWithValue(Adres.ParamMiejscowosc, miejscowosc);

                komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

                return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
            }
        }

        public static long EdytujAdres(MySqlConnection polaczenie,
            int nrLokalu,
            string kraj,
            string kodPocztowy,
            string powiat,
            string wojewodztwo,
            int nrMieszkania,
            string ulica,
            int numer,
            string miejscowosc,
            int idAdres)
        {
            // Compose query using sql parameters
            string polecenie_sql = "UPDATE " + Adres.NazwaTabeli + 
                   " SET " + Adres.NrLokalu + "=" + Adres.ParamNrLokalu + "," +
                   Adres.Kraj + "=" + Adres.ParamKraj + "," +
                   Adres.KodPocztowy + "=" + Adres.ParamKodPocztowy + "," +
                   Adres.Powiat + "=" + Adres.ParamPowiat + "," +
                   Adres.Wojewodztwo + "=" + Adres.ParamWojewodztwo + "," +
                   Adres.NrMieszkania + "=" + Adres.ParamNrMieszkania + "," +
                   Adres.Ulica + "=" + Adres.ParamUlica + "," +
                   Adres.Numer + "=" + Adres.ParamNumer + "," +
                   Adres.Miejscowosc + "=" + Adres.ParamMiejscowosc +
                   " WHERE " + Adres.IdAdres + "=" + Adres.ParamIdAdres;

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(Adres.ParamNrLokalu, nrLokalu);  //AddWithValue --Dodaje wartość do końca SqlParameterCollection.
                komenda.Parameters.AddWithValue(Adres.ParamKraj, kraj);
                komenda.Parameters.AddWithValue(Adres.ParamKodPocztowy, kodPocztowy);
                komenda.Parameters.AddWithValue(Adres.ParamPowiat, powiat);
                komenda.Parameters.AddWithValue(Adres.ParamWojewodztwo, wojewodztwo);
                komenda.Parameters.AddWithValue(Adres.ParamNrMieszkania, nrMieszkania);
                komenda.Parameters.AddWithValue(Adres.ParamUlica, ulica);
                komenda.Parameters.AddWithValue(Adres.ParamNumer, numer);
                komenda.Parameters.AddWithValue(Adres.ParamMiejscowosc, miejscowosc);
                komenda.Parameters.AddWithValue(Adres.ParamIdAdres, idAdres); 

                komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

                return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
            }
        }
    }
}