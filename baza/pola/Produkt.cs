﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class Produkt
    {
        public static readonly String NazwaTabeli = "produkt";

        public static readonly String IdProduktu = "id_produktu";

        public static readonly String NazwaProduktu = "nazwa_produktu";
        public static readonly String IloscWMagazynie = "ilosc_w_magazynie";
        public static readonly String Wersja = "wersja";
        public static readonly String Opis = "opis";
        public static readonly String CenaBrutto = "cena_brutto";
        public static readonly String Ocena = "ocena";
        public static readonly String Producent = "producent";
        public static readonly String Kategoria = "kategoria";
        public static readonly String Vat = "vat";
        public static readonly String Promocje = "promocje";

        public static readonly String ParamIdProduktu = '@' + IdProduktu;

        public static readonly String ParamNazwaProduktu = '@' + NazwaProduktu ;
        public static readonly String ParamIloscWMagazynie = '@' + IloscWMagazynie ;
        public static readonly String ParamWersja = '@' + Wersja ;
        public static readonly String ParamOpis = '@' + Opis ;
        public static readonly String ParamCenaBrutto =  '@' + CenaBrutto ;
        public static readonly String ParamOcena =  '@' + Ocena;
        public static readonly String ParamProducent =  '@' + Producent;
        public static readonly String ParamKategoria = '@' + Kategoria ;
        public static readonly String ParamVat = '@' + Vat ;
        public static readonly String ParamPromocje = '@' + Promocje ;

        public static readonly String WszystkieNazwyPol =
            NazwaProduktu + ',' +
            IloscWMagazynie + ',' +
            Wersja + ',' +
            Opis + ',' +
            CenaBrutto + ',' +
            Producent + ',' +
            Kategoria + ',' +
            Vat + ',' +
            Promocje;

        public static readonly String WszystkieNazwyParametrow =
            ParamNazwaProduktu + ',' +
            ParamIloscWMagazynie + ',' +
            ParamWersja + ',' +
            ParamOpis + ',' +
            ParamCenaBrutto + ',' +
            ParamProducent + ',' +
            ParamKategoria + ',' +
            ParamVat + ',' +
            ParamPromocje;






        public static bool SprawdzProdukt(MySqlConnection polaczenie, string NazwaProduktu, string Wersja)
        {
            string polecenie_sql = "SELECT " + Produkt.IdProduktu +
                " FROM " + Produkt.NazwaTabeli + " WHERE " + Produkt.NazwaProduktu + "=" + Produkt.ParamNazwaProduktu + " AND " + Produkt.Wersja + "=" + Produkt.ParamWersja;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Produkt.ParamNazwaProduktu, NazwaProduktu);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count != 0)
            {
                return false;
            }

            return true;
        }



        
internal static long DodajProdukt(MySqlConnection polaczenie,
    string NazwaProduktu,
    int IloscwMagazynie,
    string Wersja,
    string Opis,
    float CenaBrutto,
    int Producent,
    int Vat,
    int Promocje,
    int Kategoria)
{
    // Compose query using sql parameters
    string polecenie_sql = "INSERT INTO " + NazwaTabeli +
        " (" + WszystkieNazwyPol + ") " +
        "values (" + WszystkieNazwyParametrow + ")";


    using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
    {
        komenda.Parameters.AddWithValue(Produkt.ParamNazwaProduktu, NazwaProduktu);  //AddWithValue --Dodaje wartość do końca SqlParameterCollection.
        komenda.Parameters.AddWithValue(Produkt.ParamIloscWMagazynie, IloscwMagazynie);
        komenda.Parameters.AddWithValue(Produkt.ParamWersja, Wersja);
        komenda.Parameters.AddWithValue(Produkt.ParamOpis, Opis);
        komenda.Parameters.AddWithValue(Produkt.ParamCenaBrutto, CenaBrutto);
        komenda.Parameters.AddWithValue(Produkt.ParamProducent, Producent);
        komenda.Parameters.AddWithValue(Produkt.ParamKategoria, Kategoria);
        komenda.Parameters.AddWithValue(Produkt.ParamVat, Vat);
        komenda.Parameters.AddWithValue(Produkt.ParamPromocje, Promocje);
   

        komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

        return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
    }
}
    }
}