﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public static class Osoba
    {
        public static readonly String NazwaTabeli = "Osoba";

        public static readonly String IdOsoba = "id_osoba"; 

        public static readonly String Imie = "imie";
        public static readonly String Nazwisko = "nazwisko";
        public static readonly String Login = "login";
        public static readonly String Haslo = "haslo";
        public static readonly String DataDodaniaKlienta = "data_dodania_klienta";
        public static readonly String Pracownik = "pracownik";
        public static readonly String Adres = "adres";

        public static readonly String ParamImie = '@'+Imie;
        public static readonly String ParamNazwisko = '@' + Nazwisko;
        public static readonly String ParamLogin = '@' + Login;
        public static readonly String ParamHaslo = '@' + Haslo;
        public static readonly String ParamDataDodaniaKlienta = '@'+ DataDodaniaKlienta;
        public static readonly String ParamPracownik = '@'+ Pracownik;
        public static readonly String ParamAdres = '@'+ Adres;

        public static readonly String WszystkieNazwyPol =
            Imie + ',' +
            Nazwisko + ',' +
            Login + ',' +
            Haslo + ',' +
            DataDodaniaKlienta + ',' +
            Pracownik + ',' +
            Adres;

        public static readonly String WszystkieNazwyParametrow =
            ParamImie + ',' +
            ParamNazwisko + ',' +
            ParamLogin + ',' +
            ParamHaslo + ',' +
            ParamDataDodaniaKlienta + ',' +
            ParamPracownik + ',' +
            ParamAdres;

		public static  long DodajOsoba(MySqlConnection polaczenie,
			string imie,
			string nazwisko,
			string login,
			string haslo,
			bool pracownik,
			long id_adres)
		{
			// Compose query using sql parameters
			string polecenie_sql = "INSERT INTO " + Osoba.NazwaTabeli +
				" (" + Osoba.Imie + ',' +
				Osoba.Nazwisko + ',' +
				Osoba.Login + ',' +
				Osoba.Haslo + ',' +
				Osoba.Pracownik + ',' +
				Osoba.Adres +
				") values (" +
				Osoba.ParamImie + ',' +
				Osoba.ParamNazwisko + ',' +
				Osoba.ParamLogin + ',' +
				Osoba.ParamHaslo + ',' +
				Osoba.ParamPracownik + ',' +
				Osoba.ParamAdres + ")";

			using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
			{
				komenda.Parameters.AddWithValue(Osoba.ParamImie, imie);
				komenda.Parameters.AddWithValue(Osoba.ParamNazwisko, nazwisko);
				komenda.Parameters.AddWithValue(Osoba.ParamLogin, login);
				komenda.Parameters.AddWithValue(Osoba.ParamHaslo, haslo);
				komenda.Parameters.AddWithValue(Osoba.ParamPracownik, pracownik);
				komenda.Parameters.AddWithValue(Osoba.ParamAdres, id_adres);

				komenda.ExecuteNonQuery();

				return komenda.LastInsertedId;
			}
		}

		public static bool SprawdzLogin(MySqlConnection polaczenie, string login)
        {
            string polecenie_sql = "SELECT " + Osoba.IdOsoba +
                " FROM " + Osoba.NazwaTabeli + " WHERE " + Osoba.Login + "=" + Osoba.ParamLogin;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(Osoba.ParamLogin, login);

            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count != 0)
            {
                return false;
            }

            return true;
        }

        public static long EdytujOsoba(MySqlConnection polaczenie,
            string haslo,
            string login)
        {
            // Compose query using sql parameters
            string polecenie_sql = "UPDATE " + Osoba.NazwaTabeli +
                  " SET " + Osoba.Haslo + "=" + Osoba.ParamHaslo +
                  " WHERE "  + Osoba.Login + "=" + Osoba.ParamLogin;

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(Osoba.ParamLogin, login);
                komenda.Parameters.AddWithValue(Osoba.ParamHaslo, haslo);
          
                komenda.ExecuteNonQuery();

                return komenda.LastInsertedId;
            }
        }
    }
}