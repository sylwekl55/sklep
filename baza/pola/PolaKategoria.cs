﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
	public class PolaKategoria
	{
		public static readonly String NazwaTabeli = "Kategoria";

		public static readonly String IdKategoria = "id_kategoria";
		public static readonly String Nazwa = "nazwa";
		public static readonly String Bazowa = "bazowa";

		public static readonly String ParamNazwa = '@' + Nazwa;
		public static readonly String ParamBazowa = '@' + Bazowa;

		public static readonly String WszystkieNazwyPol =
			Nazwa + ',' +
			Bazowa;

		public static readonly String WszystkieNazwyParametrow =
			ParamNazwa + ',' +
			ParamBazowa;

        public static long DodajKategorie(MySqlConnection polaczenie,
            string nazwa,
            int bazowa)
        {
            string polecenie_sql = "INSERT INTO " + PolaKategoria.NazwaTabeli +
                " (" + PolaKategoria.WszystkieNazwyPol + ") " +
                "values (" + PolaKategoria.WszystkieNazwyParametrow + ")";

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(PolaKategoria.ParamNazwa, nazwa);
                komenda.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa);

                komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

                return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
            }
            
        }

        public static long DodajKategorieBazowe(MySqlConnection polaczenie,string nazwa)
        {
            string polecenie_sql = "INSERT INTO " + PolaKategoria.NazwaTabeli +
                " (" + PolaKategoria.Nazwa + ") " +
                "values (" + PolaKategoria.ParamNazwa+ ")";

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(PolaKategoria.ParamNazwa, nazwa);

                komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

                return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
            }

        }
        public static long UsunKategorie(MySqlConnection polaczenie,int bazowa)
        {
            string polecenie_sql = "DELETE FROM " + PolaKategoria.NazwaTabeli +
                " WHERE " + PolaKategoria.IdKategoria + " = " + PolaKategoria.ParamBazowa;

            using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
            {
                komenda.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa);

                komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

                return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
            }

        }

        public static bool CzyKategoriaNieMaPodkategorii(MySqlConnection polaczenie, int bazowa)
        {
            string polecenie_sql = "SELECT " + PolaKategoria.Bazowa +
                " FROM " + PolaKategoria.NazwaTabeli + " WHERE " + PolaKategoria.Bazowa + "=" + PolaKategoria.ParamBazowa;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa);

            System.Data.DataTable tabela = new System.Data.DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count != 0)
            {
                return false;
            }

            return true;
        }

        public static bool CzyBrakProduktowWKategorii(MySqlConnection polaczenie, int bazowa)
        {
            string polecenie_sql = "SELECT " + Produkt.Kategoria +
                " FROM " + Produkt.NazwaTabeli + " WHERE " + Produkt.Kategoria + "=" + PolaKategoria.ParamBazowa;

            MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
            adapter.SelectCommand.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa);

            System.Data.DataTable tabela = new System.Data.DataTable();
            adapter.Fill(tabela);

            if (tabela.Rows.Count != 0)
            {
                return false;
            }

            return true;
        }



        public static bool EdytujKategorie(MySqlConnection polaczenie, int bazowa, string nazwa)
        {
            {
                string polecenie_sql = "UPDATE " + PolaKategoria.NazwaTabeli +
                    " SET " + PolaKategoria.Nazwa + "=" + PolaKategoria.ParamNazwa + " WHERE " + PolaKategoria.IdKategoria + "=" + PolaKategoria.ParamBazowa;

                MySqlDataAdapter adapter = new MySqlDataAdapter(polecenie_sql, polaczenie);
                adapter.SelectCommand.Parameters.AddWithValue(PolaKategoria.ParamNazwa, nazwa);
                adapter.SelectCommand.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa);

                System.Data.DataTable tabela = new System.Data.DataTable();
                adapter.Fill(tabela);

                if (tabela.Rows.Count != 0)
                {
                    return false;
                }

                return true;
            }
        }
    }
}