﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class Pracownik
    {
        public static readonly String NazwaTabeli = "Pracownik";

        public static readonly String IdPracownika = "id_pracownika";

        public static readonly String NumerSluzbowy = "numer_sluzbowy";
        public static readonly String DataZatrudnienia = "data_zatrudnienia";
        public static readonly String DataZwolnienia = "data_zwolnienia";
        public static readonly String Osoba = "osoba";
        public static readonly String DodajePracownikow = "dodaje_pracownikow";

        public static readonly String ParamNumerSluzbowy = '@' + NumerSluzbowy;
        public static readonly String ParamDataZatrudnienia = '@' + DataZatrudnienia;
        public static readonly String ParamDataZwolnienia = '@' + DataZwolnienia;
        public static readonly String ParamOsoba = '@' + Osoba;
        public static readonly String ParamDodajePracownikow = '@' + DodajePracownikow;


        public static readonly String WszystkieNazwyPol =
            NumerSluzbowy + ',' +
            DataZatrudnienia + ',' +
            DataZwolnienia + ',' +
            Osoba+ ',' +
            DodajePracownikow;
      
        public static readonly String WszystkieNazwyParametrow =
            ParamNumerSluzbowy + ',' +
            ParamDataZatrudnienia + ',' +
            ParamDataZwolnienia + ',' +
            ParamOsoba+ ',' +
            ParamDodajePracownikow;

        public static long DodajPracownika( MySql.Data.MySqlClient.MySqlConnection polonczenie,
            int numerSluzbowy,
            long osoba,
            bool dodajePracownikow)
        {
            string polonczenie_sql = "INSERT INTO " + Pracownik.NazwaTabeli +
                " (" + Pracownik.NumerSluzbowy + ", " +
                Pracownik.Osoba + ", " +
                Pracownik.DodajePracownikow +
                ") values (" +
                Pracownik.ParamNumerSluzbowy + ", " +
                Pracownik.ParamOsoba + ", " +
                Pracownik.ParamDodajePracownikow + ")";

            using (MySql.Data.MySqlClient.MySqlCommand komenda = new MySql.Data.MySqlClient.MySqlCommand(polonczenie_sql, polonczenie) )
            {
                komenda.Parameters.AddWithValue(Pracownik.ParamNumerSluzbowy, numerSluzbowy);
                komenda.Parameters.AddWithValue(Pracownik.ParamOsoba, osoba);
                komenda.Parameters.AddWithValue(Pracownik.ParamDodajePracownikow, dodajePracownikow);
                komenda.ExecuteNonQuery();

                return komenda.LastInsertedId;
            }
        }
    }
}