﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public class Producent
    {
        public static readonly String NazwaTabeli = "Producent";

        public static readonly String IdProducent = "id_producent";

        public static readonly String NazwaFirmy = "nazwa_firmy";
        public static readonly String NIP = "nip";
        public static readonly String Regon = "regon";
        public static readonly String Adres = "adres";

        public static readonly String ParamNazwaFirmy = '@' + NazwaFirmy;
        public static readonly String ParamNIP = '@' + NIP;
        public static readonly String ParamRegon = '@' + Regon;
        public static readonly String ParamAdres = '@' + Adres;

        public static readonly String WszystkieNazwyPol =
            NazwaFirmy + ',' +
            NIP + ',' +
            Regon + ',' +
            Adres;

        public static readonly String WszystkieNazwyParametrow =
            ParamNazwaFirmy + ',' +
            ParamNIP + ',' +
            ParamRegon + ',' +
            ParamAdres;


		public static long DodajProducentaa(MySqlConnection polaczenie,
						string NazwaFirmy,
						int NIP,
						int Regon,
						long id_adres
						)
		{
			string polecenie_sql = "INSERT INTO " + NazwaTabeli +
                " (" + WszystkieNazwyPol + ") " +
                "values (" + WszystkieNazwyParametrow + ")";


			using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
			{
				komenda.Parameters.AddWithValue(Producent.ParamNazwaFirmy, NazwaFirmy);  //AddWithValue --Dodaje wartość do końca SqlParameterCollection.
				komenda.Parameters.AddWithValue(Producent.ParamNIP, NIP);
				komenda.Parameters.AddWithValue(Producent.ParamRegon, Regon);
				komenda.Parameters.AddWithValue(Osoba.ParamAdres, id_adres);

				komenda.ExecuteNonQuery();// włąściwe wstawianie danych d o bazy 

				return komenda.LastInsertedId; // zwraca id dodaneg rekodrdu (adresu)
			}
		}
    }    
}