﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza.pola
{
    public static class Klient
    {
        public static readonly String NazwaTabeli = "Klient";

        public static readonly String IdKlient = "id_klient";

        public static readonly String Osoba = "osoba";
        public static readonly String NazwaFirmy = "nazwa_firmy";
        public static readonly String Regon = "regon";
        public static readonly String Nip = "nip";

        public static readonly String ParamOsoba = '@'+Osoba;
        public static readonly String ParamNazwaFirmy = '@' + NazwaFirmy;
        public static readonly String ParamRegon = '@' + Regon;
        public static readonly String ParamNip = '@' + Nip;

        public static readonly String WszystkieNazwyPol =
            Osoba + ',' +
            NazwaFirmy + ',' +
            Regon + ',' +
            Nip;

        public static readonly String WszystkieNazwyParametrow =
            ParamOsoba + ',' +
            ParamNazwaFirmy + ',' +
            ParamRegon + ',' +
            ParamNip;
    }
}