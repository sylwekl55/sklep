﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.baza
{
    public class UsunKategorie
    {
        public int Bazowa { get; set; }
        public String PowodNieUdanegoUsuniecia { get; private set; }
        
        public bool UsuwanieKategorii()
        {
            try
            {
                using (MySql.Data.MySqlClient.MySqlConnection polaczenie = Baza.UtworzPolaczenie())
                {
                    polaczenie.Open();

                    if (!pola.PolaKategoria.CzyBrakProduktowWKategorii(polaczenie, Bazowa ))
                    {
                        PowodNieUdanegoUsuniecia = "Nie Można usunąć kategorii z powodu że znajdują się istniejące produkty danej kategorii";
                        return false;
                    }

                    if (!pola.PolaKategoria.CzyKategoriaNieMaPodkategorii(polaczenie, Bazowa))
                    {
                        PowodNieUdanegoUsuniecia = "dana kategoria ma podkategorie najpierw trzeba je usunąć ";
                        return false;
                    }

                    long id_kategorii = sklepmaster.baza.pola.PolaKategoria.UsunKategorie(polaczenie,
                        Bazowa);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
            }
        }
    }
}