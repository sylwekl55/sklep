﻿using MySql.Data.MySqlClient;
using sklepmaster.baza.pola;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster.Account
{
    public class RejestracjaPracownikow
    {
        public int NrLokalu { get; set; }
        public String Kraj { get; set; }
        public String KodPocztowy { get; set; }
        public String Powiat { get; set; }
        public String Wojewodztwo { get; set; }
        public int NrMieszkania { get; set; }
        public String Ulica { get; set; }
        public String Email { get; set; }

        /// <summary>
        /// Numer telefonu
        /// </summary>
        public int Numer { get; set; }

        public String Miejscowosc { get; set; }

        public String Imie { get; set; }
        public String Nazwisko { get; set; }
        public String Login { get; set; }
        public String Haslo { get; set; }

        public String NazwaFirmy { get; set; }
        public String Regon { get; set; }
        public String Nip { get; set; }

        public int NumerSluzbowy { get; set; }
        public String DodajePracownikow { get; set; } 
        public String PowodNieZarejestrowania { get; private set; }

		public bool Rejestruj()
		{

			try
			{
				using (MySqlConnection polaczenie = Baza.UtworzPolaczenie())
				{
					// Otwórz połączenie
					polaczenie.Open();

					if (!Osoba.SprawdzLogin(polaczenie, Login))
					{
						PowodNieZarejestrowania = "Podany login już zajęty";

						return false;
					}

					long id_adres = Adres.DodajAdres(polaczenie,
						NrLokalu,
						Kraj,
						KodPocztowy,
						Powiat,
						Wojewodztwo,
						NrMieszkania,
						Ulica,
						Email,
						Numer,
						Miejscowosc);
					long id_osoba = Osoba.DodajOsoba(polaczenie,
						Imie,
						Nazwisko,
						Login,
						Haslo,
						true,
						id_adres);
                    long pracownik = Pracownik.DodajPracownika(polaczenie,
                        NumerSluzbowy,
                        id_osoba,
                        true);
					_DodajPracownik(polaczenie, id_osoba);
					return true;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Cannot insert record [" + ex.Message + "]", ex);
			}
		}

		private void _DodajPracownik(MySqlConnection polaczenie, long id_osoba)
		{
			// Compose query using sql parameters
			string polecenie_sql = "INSERT INTO " + Pracownik.NazwaTabeli +
				" (" + Pracownik.NumerSluzbowy + ',' + Pracownik.Osoba + ',' + Pracownik.DodajePracownikow +
				") values (" + Pracownik.ParamNumerSluzbowy + ',' + Pracownik.ParamOsoba + ',' + Pracownik.ParamDodajePracownikow + ")";

			using (MySqlCommand komenda = new MySqlCommand(polecenie_sql, polaczenie))
			{
				komenda.Parameters.AddWithValue(Pracownik.ParamNumerSluzbowy, NumerSluzbowy);
				komenda.Parameters.AddWithValue(Pracownik.ParamOsoba, id_osoba);
				komenda.Parameters.AddWithValue(Pracownik.ParamDodajePracownikow, DodajePracownikow);
				

				komenda.ExecuteNonQuery();
			}
		}
    }
}