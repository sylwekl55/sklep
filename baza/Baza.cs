﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepmaster
{
    public class Baza
    {
        private static readonly string DANE_POLACZENIA =
            "SERVER=localhost;" +
            "PORT=3306;" +
            "DATABASE=sklep;" +
            "UID=sylwek;" +
            "PASSWORD=sylwekl5";

        public static MySqlConnection UtworzPolaczenie()
        {
            return new MySqlConnection(DANE_POLACZENIA);
        }
    }
}