﻿using MySql.Data.MySqlClient;
using sklepmaster.baza.pola;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepmaster
{
	public class WezelKategorii
	{
		public Kategoria Bazowa { get; private set; }
		public Kategoria Aktualna { get; private set; }
		public List<WezelKategorii> Podkategorie { get; set; }

		public TreeNode TreeWezel { get; private set; }

		public WezelKategorii(Kategoria bazowa, Kategoria aktualna)
		{
			this.Bazowa = bazowa;
			this.Aktualna = aktualna;

			this.TreeWezel = new TreeNode(aktualna.Nazwa);
			this.TreeWezel.Value = aktualna.Id.ToString();
		}

		public static List<WezelKategorii> PobierzWszystkie(MySqlConnection polaczenie, Kategoria bazowa = null)
		{
			string polecenie_sql = "SELECT " + PolaKategoria.IdKategoria + ',' + PolaKategoria.Nazwa +
				" FROM " + PolaKategoria.NazwaTabeli + " WHERE " + PolaKategoria.Bazowa;

			if (bazowa == null)
			{
				polecenie_sql += " is NULL";
			}
			else
			{
				polecenie_sql += "=" + PolaKategoria.ParamBazowa;
			}

			List<WezelKategorii> listaKategorii = new List<WezelKategorii>();

			using(MySqlCommand cmd = new MySqlCommand(polecenie_sql, polaczenie))
			{
				if (bazowa != null)
				{
					cmd.Parameters.AddWithValue(PolaKategoria.ParamBazowa, bazowa.Id);
				}


				using (MySqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Kategoria aktualna = new Kategoria();

						aktualna.Nazwa = reader.GetString(PolaKategoria.Nazwa);
						aktualna.Id = reader.GetInt32(PolaKategoria.IdKategoria);

						WezelKategorii wezel = new WezelKategorii(bazowa, aktualna);
						listaKategorii.Add(wezel);
					}
				}

				foreach (WezelKategorii wezel in listaKategorii)
				{
					wezel.Podkategorie = PobierzWszystkie(polaczenie, wezel.Aktualna);

					foreach(WezelKategorii podkategoria in wezel.Podkategorie)
					{
						TreeNode node = podkategoria.TreeWezel;

						wezel.TreeWezel.ChildNodes.Add(node);
					}
				}
			}

			return listaKategorii;
		}
	}
}